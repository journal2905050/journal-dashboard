<?php

use App\Http\Controllers\Auth\AuthenticationController;
use App\Http\Controllers\Helper\DropdownController;
use App\Http\Controllers\Modules\BkPresenceController;
use App\Http\Controllers\Modules\ClassroomController;
use App\Http\Controllers\Modules\EmployeeController;
use App\Http\Controllers\Modules\EmployeeJournalController;
use App\Http\Controllers\Modules\PeriodeController;
use App\Http\Controllers\Modules\StudentClassController;
use App\Http\Controllers\Modules\StudentController;
use App\Http\Controllers\Students\PresenceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.auth');
// });



Route::group(['prefix' => ''], function () {
    Route::get('', [AuthenticationController::class, 'index'])->name('login');
    Route::post('', [AuthenticationController::class, 'signIn']);
    Route::get('logout', [AuthenticationController::class, 'signOut']);
    Route::post('change-password', [AuthenticationController::class, 'changePassword']);
});

Route::group(['prefix' => 'dropdown'], function () {
    Route::get('gender', [DropdownController::class, 'gender']);
    Route::get('grade', [DropdownController::class, 'grade']);
    Route::get('school-year', [DropdownController::class, 'school_year']);
    Route::get('classroom', [DropdownController::class, 'classroom']);
    Route::get('batch-in-exist', [DropdownController::class, 'batch_in_exist']);
    Route::get('journal-hour', [DropdownController::class, 'journal_hour']);
});

Route::group(['prefix' => 'periode'], function () {
    Route::get('', [PeriodeController::class, 'index']);
    Route::get('get', [PeriodeController::class, 'get']);
    Route::post('add', [PeriodeController::class, 'store']);
    Route::post('activation/{id}', [PeriodeController::class, 'activation']);
    Route::delete('{id}', [PeriodeController::class, 'remove']);
});

Route::group(['prefix' => 'employee'], function () {
    Route::get('', [EmployeeController::class, 'index']);
    Route::get('get', [EmployeeController::class, 'get']);
    Route::get('show/{id}', [EmployeeController::class, 'show']);
    Route::post('add', [EmployeeController::class, 'store']);
    Route::post('edit/{id}', [EmployeeController::class, 'edit']);
});

Route::group(['prefix' => 'student'], function () {
    Route::get('', [StudentController::class, 'index']);
    Route::get('get', [StudentController::class, 'get']);
    Route::get('show/{id}', [StudentController::class, 'show']);
    Route::post('add', [StudentController::class, 'store']);
    Route::post('edit/{id}', [StudentController::class, 'edit']);
});

Route::group(['prefix' => 'classroom'], function () {
    Route::get('', [ClassroomController::class, 'index']);
    Route::get('get', [ClassroomController::class, 'get']);
    Route::get('show/{id}', [ClassroomController::class, 'show']);
    Route::post('add', [ClassroomController::class, 'store']);
    Route::post('edit/{id}', [ClassroomController::class, 'edit']);
    Route::delete('delete/{id}', [ClassroomController::class, 'remove']);
});

Route::group(['prefix' => 'student-class'], function () {
    Route::get('', [StudentClassController::class, 'index']);
    Route::get('get', [StudentClassController::class, 'get']);
    Route::get('show/{id}', [StudentClassController::class, 'show']);
    Route::post('add', [StudentClassController::class, 'store']);
    Route::post('edit/{id}', [StudentClassController::class, 'edit']);
    Route::delete('delete/{id}', [StudentClassController::class, 'remove']);
});

Route::group(['prefix' => 'student-mutation'], function () {
    Route::get('', [StudentClassController::class, 'studentMutation']);
    Route::post('add', [StudentClassController::class, 'studentMutationStore']);
});

Route::group(['prefix' => 'employee-journal'], function () {
    Route::get('', [EmployeeJournalController::class, 'index']);
    Route::get('get', [EmployeeJournalController::class, 'get']);
    Route::get('show/{id}', [EmployeeJournalController::class, 'show']);
    Route::post('add', [EmployeeJournalController::class, 'store']);
    Route::post('presence/{id}', [EmployeeJournalController::class, 'presence']);
});


Route::group(['prefix' => 'bk-presence'], function () {
    Route::get('', [BkPresenceController::class, 'index']);
    Route::get('get', [BkPresenceController::class, 'get']);
    Route::get('show/{id}', [BkPresenceController::class, 'show']);
    Route::post('edit/{id}', [BkPresenceController::class, 'edit']);
});

Route::group(['prefix' => 'student-presence'], function () {
    Route::get('', [PresenceController::class, 'index']);
    Route::get('get', [PresenceController::class, 'get']);
});


