<?php

use App\Models\Data\AcademicPeriode;
use App\Models\Data\Classroom;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $period = AcademicPeriode::create([
            'school_year' => '2023/2024',
            'is_active' => true,
        ]);
        Classroom::insert([
            ['id' => 'a3499dda-aedc-d6e0-7d41-71a0ba961149', 'name' => 'X 1', 'grade' => '10', 'school_year' => '2023/2024'],
            ['id' => '49b080a2-5a9f-29c9-1c65-cd031e345cc6', 'name' => 'X 2', 'grade' => '10', 'school_year' => '2023/2024'],
            ['id' => '70084b9e-fb6c-9121-8ff2-4c800cad41d7', 'name' => 'X 3', 'grade' => '10', 'school_year' => '2023/2024'],
            ['id' => '8fe9bc12-5183-eeda-b904-fa84eedf103c', 'name' => 'X 4', 'grade' => '10', 'school_year' => '2023/2024'],
            ['id' => 'adf3afdd-3ce8-3a22-ea19-90836e042892', 'name' => 'X 5', 'grade' => '10', 'school_year' => '2023/2024'],
            ['id' => '98839cf7-385b-ccda-7369-a919f98e0e63', 'name' => 'X 6', 'grade' => '10', 'school_year' => '2023/2024'],
            ['id' => '93ead124-d2e2-1884-9bf9-490a8f7c4ad8', 'name' => 'X 7', 'grade' => '10', 'school_year' => '2023/2024'],
            ['id' => '76e31f47-f842-7618-d541-737fe10cc918', 'name' => 'X 8', 'grade' => '10', 'school_year' => '2023/2024'],
            ['id' => '34353de0-d63c-b2a2-6c90-3031aca13278', 'name' => 'X 9', 'grade' => '10', 'school_year' => '2023/2024'],
            ['id' => '346a1e74-124f-f9c5-db6b-a20b3a6631e0', 'name' => 'X 10', 'grade' => '10', 'school_year' => '2023/2024'],
            ['id' => 'efe43111-cd7a-ace4-dcd3-21a064511317', 'name' => 'X 11', 'grade' => '10', 'school_year' => '2023/2024'],
            ['id' => '6ab71498-ef88-2c1b-9105-47a4db6f3427', 'name' => 'X 12', 'grade' => '10', 'school_year' => '2023/2024'],
            ['id' => '9293aa39-55a1-0569-2df1-36f9ebb6f94b', 'name' => 'XI 1', 'grade' => '11', 'school_year' => '2023/2024'],
            ['id' => '30190a3e-0323-cd10-5c56-033ff03f7751', 'name' => 'XI 2', 'grade' => '11', 'school_year' => '2023/2024'],
            ['id' => '031d37ce-4155-fb19-9328-ea243aced096', 'name' => 'XI 3', 'grade' => '11', 'school_year' => '2023/2024'],
            ['id' => 'cc17aaf9-1dd7-f034-9cde-113738841bb9', 'name' => 'XI 4', 'grade' => '11', 'school_year' => '2023/2024'],
            ['id' => '7a50ec20-530e-8e60-361d-47f6ab0aeb9b', 'name' => 'XI 5', 'grade' => '11', 'school_year' => '2023/2024'],
            ['id' => '12dba3cf-3c30-9b0a-fc23-3008b3342e41', 'name' => 'XI 6', 'grade' => '11', 'school_year' => '2023/2024'],
            ['id' => '6ba297a6-2a51-3c33-2ba2-53a14cada0eb', 'name' => 'XI 7', 'grade' => '11', 'school_year' => '2023/2024'],
            ['id' => 'f8a98ffe-6d66-41be-db96-661d5a650793', 'name' => 'XI 8', 'grade' => '11', 'school_year' => '2023/2024'],
            ['id' => '7ebd039d-5163-f286-2f6b-f12aaef70ed2', 'name' => 'XI 9', 'grade' => '11', 'school_year' => '2023/2024'],
            ['id' => '38993f63-4ebf-3663-91b8-f036f0ca6a0b', 'name' => 'XI 10', 'grade' => '11', 'school_year' => '2023/2024'],
            ['id' => '9d2624d1-68d0-d23b-ecfa-1b68387e6d7e', 'name' => 'XI 11', 'grade' => '11', 'school_year' => '2023/2024'],
            ['id' => '96400ba8-6ced-ebb8-8a71-5d9e0f4467bd', 'name' => 'XII IPA 1', 'grade' => '12', 'school_year' => '2023/2024'],
            ['id' => 'a7e79377-3d80-e29c-cccb-dd37eda87e98', 'name' => 'XII IPA 2', 'grade' => '12', 'school_year' => '2023/2024'],
            ['id' => 'f30e9c74-e4b1-3d25-21b1-b90669074855', 'name' => 'XII IPA 3', 'grade' => '12', 'school_year' => '2023/2024'],
            ['id' => '6fbca55e-259c-a2b0-b3c7-9dd62a111864', 'name' => 'XII IPA 4', 'grade' => '12', 'school_year' => '2023/2024'],
            ['id' => '8c759640-4684-ca7f-6028-2bfc08834e8f', 'name' => 'XII IPA 5', 'grade' => '12', 'school_year' => '2023/2024'],
            ['id' => '2a19f52f-5751-8402-6cbc-fa48bfd55c26', 'name' => 'XII IPA 6', 'grade' => '12', 'school_year' => '2023/2024'],
            ['id' => 'f0ad69ab-a671-5f78-41a4-0d70917c2f12', 'name' => 'XII IPS 1', 'grade' => '12', 'school_year' => '2023/2024'],
            ['id' => '47b2207a-483a-3f11-87cf-27614bd8480f', 'name' => 'XII IPS 2', 'grade' => '12', 'school_year' => '2023/2024'],
            ['id' => 'c4f8b75b-4322-f511-b999-f79415dfc4c9', 'name' => 'XII IPS 3', 'grade' => '12', 'school_year' => '2023/2024'],
            ['id' => '91baa68f-357b-cda0-3b29-a6efad1ab4e2', 'name' => 'XII IPS 4', 'grade' => '12', 'school_year' => '2023/2024'],
            ['id' => '8c66e6b4-fe2d-815a-b935-1e82dc8d4cd1', 'name' => 'XII BHS', 'grade' => '12', 'school_year' => '2023/2024'],
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
