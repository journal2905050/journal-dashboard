<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('presence_dailies', function (Blueprint $table) {
            $table->id();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable()->useCurrentOnUpdate();

            $table->uuid('student');
            $table->date('date');
            $table->decimal('m');
            $table->decimal('i');
            $table->decimal('t');
            $table->decimal('a');
            $table->decimal('presence');
            $table->decimal('presentage')->default('100');

            $table->unique(['student', 'date']);

            $table->foreign('student')->on('students')->references('id')->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('presence_dailies');
    }
};
