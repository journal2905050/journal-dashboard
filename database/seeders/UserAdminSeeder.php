<?php

namespace Database\Seeders;

use App\Models\Data\Person;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (User::where('is_admin',true)->count() == 0) {
            $person = Person::create([
                'name' => 'Admin SMA N 1 Ambarawa',
            ]);
            User::create([
                'id' => $person->id,
                'username' => 'admin@sman1ambarawa.com',
                'password' => '123456',
                'is_admin' => true
            ]);
        }
    }
}
