<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Models\Data\Person;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasUuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'username',
        'password',
        'is_student',
        'is_employee',
        'is_admin',
        'is_bk',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'password' => 'hashed',
        'is_student' => 'boolean',
        'is_employee' => 'boolean',
        'is_admin' => 'boolean',
        'is_bk' => 'boolean',
    ];

    protected $appends = [
        'person',
    ];

    public function getPersonAttribute()
    {
        return Person::where('id', $this->attributes['id'])->select(['name', 'gender'])->first();
    }
}
