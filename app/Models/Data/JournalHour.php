<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;

class JournalHour extends Model
{
    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'journal',
        'hour',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
