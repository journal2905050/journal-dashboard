<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;

class Environments extends Model
{
    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'category',
        'value',
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];
}
