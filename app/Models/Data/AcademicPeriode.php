<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Model;

class AcademicPeriode extends Model
{
    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'school_year',
        'is_active',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $cats = [
        'is_active' => 'boolean',
    ];
}
