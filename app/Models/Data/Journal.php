<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    use HasUuids;

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'employee',
        'classroom',
        'date',
        'subject',
        'summary',
    ];
}
