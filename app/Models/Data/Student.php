<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasUuids;

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'nis',
        'batch_in',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];
}
