<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasUuids;

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'name',
        'gender',
    ];
}
