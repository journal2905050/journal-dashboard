<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class PresenceDaily extends Model
{
    use HasUuids;

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'student',
        'date',
        'm',
        'i',
        't',
        'a',
        'presence',
        'presentage',
    ];
}
