<?php

namespace App\Models\Data;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasUuids;

    protected $fillable = [
        'id',
        'created_at',
        'updated_at',
        'alias',
        'nip',
        'rank',
        'position',
        'status',
    ];

    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];
}
