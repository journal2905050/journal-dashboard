<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Response;
use App\Models\Data\Student;
use App\Models\Data\StudentClass;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class StudentClassController extends Controller
{
    function __construct()
    {
        $this->middleware('auth', ['except' => []]);
    }

    public function index()
    {
        return view('modules.student_classes.index');
    }

    public function get(Request $request)
    {
        $model = StudentClass::query()->join('classrooms', 'student_classes.classroom', 'classrooms.id')
            ->join('people', 'student_classes.student', 'people.id')
            ->join('students', 'student_classes.student', 'students.id');
        if ($request->input('school_year', false)) {
            $model = $model->where('classrooms.school_year', $request->school_year);
        }
        if ($request->input('grade', false)) {
            $model = $model->where('classrooms.grade', $request->grade);
        }
        if ($request->input('classroom', false)) {
            $model = $model->where('classrooms.id', $request->classroom);
        }
        $model = $model->select([
            'student_classes.id',
            'people.name',
            'students.nis',
            'classrooms.name as class_name',
            'classrooms.school_year',
            'classrooms.grade',
            'students.batch_in'
        ]);

        return DataTables::of($model)
            ->addColumn('action', 'modules.student_classes.datatable.action')
            ->rawColumns(['action'])
            ->make(true);
    }

    public function studentMutation(Request $request)
    {
        $model = Student::join('people', 'students.id', 'people.id')
            ->leftJoin('student_classes', function ($qons) use ($request) {
                $qons->on('students.id', 'student_classes.student');
            })
            ->leftJoin('classrooms', function ($qonc) use ($request) {
                $qonc->on('student_classes.classroom', 'classrooms.id')
                    ->where('classrooms.school_year', $request->input('school_year', null));
            })
            ->where('students.batch_in', $request->batch_in);

        // if ($request->input('classless', false)) {
        //     $model = $model->whereNull('classrooms.name');
        // }
        $model = $model->select([
            'students.id',
            'students.nis',
            'people.name',
            'classrooms.name as class_name'
        ]);

        return DataTables::of($model)
            ->addColumn('action', 'modules.student_classes.mutation.action')
            ->rawColumns(['action'])
            ->make(true);
    }

    public function studentMutationStore(Request $request)
    {
        // return Response::successApi(['data' => $request->all()]);

        $students = $request->input('students', []);
        StudentClass::join('classrooms', 'student_classes.classroom', 'classrooms.id')
            ->whereIn('student_classes.student', $students)
            ->where('classrooms.school_year', $request->school_year)
            ->delete();

        $payload = [];
        foreach ($request->students ?? [] as $key => $value) {
            $payload[] = [
                'student' => $value,
                'classroom' => $request->classroom
            ];
        }
        StudentClass::insert($payload);

        return Response::successApi();
    }
}
