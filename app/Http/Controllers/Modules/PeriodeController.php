<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Response;
use App\Models\Data\AcademicPeriode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class PeriodeController extends Controller
{
    function __construct()
    {
        $this->middleware('auth', ['except' => []]);
    }

    public function index()
    {
        return view('modules.periodes.index');
    }

    public function get(Request $request)
    {
        $model = AcademicPeriode::query();

        return DataTables::of($model)
            ->addColumn('set_active', 'modules.periodes.datatable.is-active')
            ->addColumn('action', 'modules.periodes.datatable.action')
            ->rawColumns(['set_active', 'action'])
            ->make(true);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'school_year' => ['required', 'unique:academic_periodes,school_year']
        ]);
        if ($validator->fails()) {
            return Response::failApi([
                'message' => $validator->errors()->first()
            ]);
        }
        $isActive = $request->has('is_active');
        $new = AcademicPeriode::create([
            'school_year' => $request->school_year
        ]);

        if ($isActive) {
            $this->setActive($new->id);
        }

        return Response::successApi();
    }

    public function activation($id)
    {
        $ret = $this->setActive($id);
        if ($ret) {
            return Response::successApi();
        }
        return Response::failApi();
    }

    public function remove($id)
    {
        DB::beginTransaction();
        try {
            AcademicPeriode::where('id', $id)->delete();
            DB::commit();
            return Response::successApi();
        } catch (\Throwable $th) {
            DB::rollBack();
            return Response::failApi([
                'message' => 'Data yang telah digunakan tidak dapat dihapus.'
            ]);
        }
    }

    public function setActive($id)
    {
        AcademicPeriode::whereNotNUll('id')->update(['is_active' => false]);
        AcademicPeriode::where('id', $id)->update(['is_active' => true]);

        return true;
    }
}
