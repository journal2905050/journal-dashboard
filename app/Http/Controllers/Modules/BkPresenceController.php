<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Response;
use App\Http\Controllers\Services\PresenceService;
use App\Models\Data\Person;
use App\Models\Data\Presence;
use App\Models\Data\StudentClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BkPresenceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth', ['except' => []]);
    }
    public function index()
    {
        return view('modules.bk_presences.index');
    }

    public function get(Request $request)
    {
        $classroom = $request->input('classroom', false);
        $date = $request->input('date', false);

        if (!$classroom || !$date) {
            return Response::successApi([
                'data' => [
                    'tbody' => "<td colspan='16' class='text-center'>Silakan pilih Kelas dan Tanggal</td>"
                ]
            ]);
        }

        $student_class = StudentClass::join('students', 'student_classes.student', 'students.id')
            ->join('people', 'students.id', 'people.id')
            ->leftJoin('presence_dailies', function ($qpd) use ($date) {
                $qpd->on('students.id', 'presence_dailies.student')
                    ->where('presence_dailies.date', $date);
            })
            ->where("student_classes.classroom", $classroom)->select([
                "students.id", "students.nis", "people.name",
                DB::raw("round(coalesce(presence_dailies.presentage,null)) as presentage")
            ])->get();

        $ret = "";
        foreach ($student_class as $ksc => $vsc) {
            $ret .= "<tr id='row-presence-" . $vsc->id . "'>";
            $ret .= '<td>
                        <button id="btn-show-presence|' . $vsc->id . '"
                            type="button"
                            class="btn btn-icon btn-primary mr-1 mb-1 mt-2 btn-inline btn-show-presence">
                                <i class="feather icon-eye"></i>
                        </button>
                    </td>';
            $ret .= "<td>" . $vsc->nis . "</td>";
            $ret .= "<td>" . $vsc->name . "</td>";
            $ret .= "<td>" . $vsc->presentage . '%' ?? '-' . "/td>";

            for ($i = 1; $i <= 12; $i++) {
                $tempRet = Presence::where(['date' => $date, 'student' => $vsc->id, 'hour' => $i])->first();
                if ($tempRet) {
                    $ret .= '<td><div id="presence|' . $tempRet->id . '" class="div-presence text-center px-1 rounded">' . $tempRet->type . '</div></td>';
                } else {
                    $ret .= '<td><div id="bk|' . Str::uuid() . '" class="div-presence text-center px-1 rounded">X</div></td>';
                }
            }

            $ret .= "</tr>";
        }

        return Response::successApi([
            'data' => ['tbody' => $ret]
        ]);
    }

    public function show($id, Request $request)
    {
        $people = Person::join('students', 'people.id', 'students.id')
            ->select([
                'people.name',
                'students.nis'
            ])->where('people.id', $id)
            ->first();
        $presence = Presence::join('journals', 'presences.journal', 'journals.id')
            ->join('people', 'journals.employee', 'people.id')
            ->join('classrooms', 'journals.classroom', 'classrooms.id')
            ->select([
                'presences.id',
                'presences.hour',
                DB::raw("coalesce(presences.type,'X') as type"),
                'people.name as employee_name',
                'journals.subject',
                'classrooms.name as class_name'
            ])->where([
                'presences.student' => $id,
                'presences.date' => $request->date
            ])->get();
        $tbody = "";
        foreach ($presence as $key => $value) {
            $tbody .= "<tr>";
            $tbody .=   '<td><input type="hidden" name="presence[' . $key . '][id]" value="' . $value->id . '">' . $value->hour . '</td>';
            $tbody .= '<td>' . $this->tbodyType($value->type, $key) . '</td>';
            $tbody .= '<td>' . $value->subject . '</td>';
            $tbody .= '<td>' . $value->employee_name . '</td>';
            $tbody .= '<td>' . $value->class_name . '</td>';
            $tbody .= "</tr>";
        }

        return Response::successApi([
            'data' => [
                'student' => $people,
                'tbody' => $tbody
            ]
        ]);
    }

    public function tbodyType($type, $key)
    {
        $arr = ['X', 'M', 'I', 'T', 'A'];
        $readonly = 'disabled="true"';
        if (Auth::user()->is_bk) {
            $readonly = 'null';
        }
        $ret = '<select class="form-controll" name="presence[' . $key . '][type]" ' . $readonly . '>';

        foreach ($arr as $k => $value) {
            $selected = '';
            if ($value == $type) {
                $selected = 'selected';
            }
            $ret .= '<option value="' . $value . '" ' . $selected . '>' . $value . '</option>';
        }

        $ret .= '</select>';

        return $ret;
    }

    public function edit($id, Request $request)
    {
        $payload = $request->input('presence', []);
        foreach ($payload as $key => $value) {
            $value = (object) $value;
            Presence::where('id', $value->id)->update([
                'type' => $value->type
            ]);
        }
        PresenceService::trigerStudent($request->date, [$id]);

        return Response::successApi();
    }
}
