<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Response;
use App\Http\Controllers\Services\PresenceService;
use App\Models\Data\Classroom;
use App\Models\Data\Employee;
use App\Models\Data\Journal;
use App\Models\Data\JournalHour;
use App\Models\Data\Presence;
use App\Models\Data\StudentClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class EmployeeJournalController extends Controller
{
    function __construct()
    {
        $this->middleware('auth', ['except' => []]);
    }
    public function index()
    {
        return view('modules.employee_journal.index');
    }

    public function get(Request $request)
    {
        $model = Journal::join('journal_hours', 'journals.id', 'journal_hours.journal')
            ->join('classrooms', 'journals.classroom', 'classrooms.id')
            ->where([
                'journals.employee' => Auth::user()->id,
                'journals.date' => $request->input('date', Carbon::now()->format('Y-m-d'))
            ])
            ->select([
                'journals.id',
                'journals.subject',
                'classrooms.name as class_name',
                'classrooms.grade',
                DB::raw("ARRAY_TO_STRING(array_agg(journal_hours.hour),',') as hours")
            ])->groupBy([
                'journals.id',
                'journals.subject',
                'classrooms.name',
                'classrooms.grade',
            ]);

        return DataTables::of($model)
            ->addColumn('action', 'modules.employee_journal.datatable.action')
            ->rawColumns(['action'])
            ->make(true);
    }

    public function show($id, Request $request)
    {
        $journal = Journal::where('id', $id)->first();
        $journal_hour = JournalHour::where('journal', $id)->get();
        $classroom = Classroom::where('id', $journal->classroom)->first();
        $student_class = StudentClass::join('students', 'student_classes.student', 'students.id')
            ->join('people', 'students.id', 'people.id')
            ->where('classroom', $journal->classroom)
            ->select([
                'students.id',
                'students.nis',
                'people.name',
            ])
            ->get();

        $ret = [
            'school_year' => $classroom->school_year,
            'grade' => $classroom->grade,
            'classroom' => $journal->classroom,
            'class_name' => $classroom->name,
            'date' => $journal->date,
            'subject' => $journal->subject,
            'hours' => $journal_hour->pluck('hour')->toArray(),
            'hours_label' => implode(',', $journal_hour->pluck('hour')->toArray()),
            'summary' => $journal->summary,
        ];

        $th = '<tr>
                <th rowspan="2" class="align-middle text-center">Nis</th>
                <th rowspan="2" class="align-middle text-center">Nama</th>
                <th colspan="' . count($ret['hours']) . '" class="text-center">Jam Ke</th>
            </tr>';
        $th .= '<tr>';
        foreach ($journal_hour as $key => $value) {
            $th .= '<th class="text-center">' . $value->hour . '</th>';
        }
        $th .= '</tr>';

        $td = "";
        foreach ($student_class as $key => $value) {
            $td .= '<tr>';
            $td .= '<td>' . $value->nis . '</td>';
            $td .= '<td>' . $value->name . '</td>';
            foreach ($journal_hour as $k => $v) {
                $tdvalue = Presence::where([
                    'student' => $value->id,
                    'journal' => $v->journal,
                    'hour' => $v->hour
                ])->first()->type ?? 'X';
                $td .= '<td>
                        <div
                            id="' . $value->id . '|' . $v->hour . '"
                            class="div-presence text-center px-1 rounded">' . $tdvalue . '</div>
                    </td>';
            }


            $td .= "</tr>";
        }


        $table = [
            'thead' => $th,
            'tbody' => $td
        ];
        $ret['table'] = $table;
        return Response::successApi([
            'data' => $ret
        ]);
    }

    public function store(Request $request)
    {
        $employee = Employee::where('id', Auth::user()->id)->first();
        if (!$employee) {
            return Response::failApi(['message' => 'Anda tidak memiliki akses.']);
        }
        $validator = Validator::make($request->all(), [
            'classroom' => ['required', 'exists:classrooms,id'],
            'date' => ['required', 'date'],
            'subject' => ['required'],
            'hours' => ['required', 'array', 'min:1']
        ]);
        if ($validator->fails()) {
            return Response::failApi([
                'message' => $validator->errors()->first()
            ]);
        }

        DB::beginTransaction();
        try {
            $journal = Journal::create([
                'employee' => Auth::user()->id,
                'classroom' => $request->classroom,
                'date' => $request->date,
                'subject' => $request->subject,
                'summary' => $request->summary ?? null,
            ]);
            $hours = [];
            foreach ($request->hours as $key => $value) {
                $hours[] = [
                    'journal' => $journal->id,
                    'hour' => $value,
                ];
            }
            JournalHour::insert($hours);
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            return Response::failApi(['message' => $th->getMessage()]);
        }
        return Response::successApi();
    }

    public function presence($id, Request $request)
    {
        // return Response::failApi($request->all());
        $validator = Validator::make($request->all(), [
            'presences' => ['array', 'required'],
        ]);
        if ($validator->fails()) {
            return Response::failApi([
                'message' => $validator->errors()->first()
            ]);
        }
        $journal = Journal::where('id',$id)->first();

        Journal::where('id', $id)->update([
            'summary' => $request->summary
        ]);
        Presence::where('journal', $id)->delete();
        Presence::insert($request->presences);
        PresenceService::trigerClass($journal->classroom, $request->date);
        return Response::successApi();
    }
}
