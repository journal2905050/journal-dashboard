<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Response;
use App\Models\Data\Classroom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ClassroomController extends Controller
{
    function __construct()
    {
        $this->middleware('auth', ['except' => []]);
    }

    public function index()
    {
        return view('modules.classrooms.index');
    }

    public function get(Request $request)
    {
        $model = Classroom::query();
        if ($request->input('school_year', false)) {
            $model = $model->where('school_year', $request->school_year);
        }
        if ($request->input('grade', false)) {
            $model = $model->where('grade', $request->grade);
        }

        return DataTables::of($model)
            ->addColumn('action', 'modules.classrooms.datatable.action')
            ->rawColumns(['action'])
            ->make(true);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'school_year' => ['required', 'exists:academic_periodes,school_year'],
            'grade' => ['required'],
            'name' => ['required']
        ]);
        if ($validator->fails()) {
            return Response::failApi([
                'message' => $validator->errors()->first()
            ]);
        }
        Classroom::create($request->only([
            'school_year', 'grade', 'name'
        ]));

        return Response::successApi();
    }

    public function edit($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'school_year' => ['required', 'exists:academic_periodes,school_year'],
            'grade' => ['required'],
            'name' => ['required']
        ]);
        if ($validator->fails()) {
            return Response::failApi([
                'message' => $validator->errors()->first()
            ]);
        }
        Classroom::where('id', $id)->update($request->only([
            'school_year', 'grade', 'name'
        ]));

        return Response::successApi();
    }

    public function show($id)
    {
        $data = Classroom::where('id', $id)->first();

        return Response::successApi([
            'data' => $data
        ]);
    }

    public function remove($id)
    {
        DB::beginTransaction();
        try {
            Classroom::where('id', $id)->delete();
            DB::commit();
            return Response::successApi();
        } catch (\Throwable $th) {
            DB::rollBack();
            return Response::failApi([
                'message' => 'Data yang telah digunakan tidak dapat dihapus.'
            ]);
        }
    }
}
