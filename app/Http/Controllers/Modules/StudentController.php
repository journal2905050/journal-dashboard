<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Response;
use App\Models\Data\Person;
use App\Models\Data\Student;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class StudentController extends Controller
{
    function __construct()
    {
        $this->middleware('auth', ['except' => []]);
    }
    public function index()
    {
        return view('modules.students.index');
    }

    public function get(Request $request)
    {
        $model = Person::join('students', 'people.id', 'students.id')
            ->select([
                'people.id',
                'people.name',
                'people.gender',
                'students.nis',
                'students.batch_in',
            ]);

        return DataTables::of($model)
            ->addColumn('action', 'modules.students.datatable.action')
            ->rawColumns(['action'])
            ->make(true);
    }

    public function store(Request $request)
    {
        $validator = Validator($request->all(), [
            'person' => ['required', 'array'],
            'person.name' => ['required'],
            'student' => ['array'],
            'student.nip' => ['nullable', Rule::unique('student', 'nis')],
            'user' => ['array'],
            'user.username' => ['required', Rule::unique('users', 'username')],
            'user.password' => ['required', 'string', 'min:6'],
        ]);
        if ($validator->fails()) {
            return Response::failApi(['message' => $validator->errors()->first()]);
        }
        DB::beginTransaction();
        try {
            $person = Person::create($request->person);

            $student = $request->student;
            $student['id'] = $person->id;
            Student::create($student);

            $user = [
                'id' => $person->id,
                'username' => $request->input('user.username'),
                'password' => $request->input('user.password'),
                'is_student' => true,
            ];
            User::create($user);

            DB::commit();
            return Response::successApi();
        } catch (\Throwable $th) {
            DB::rollBack();
            return Response::failApi();
        }
    }

    public function show($id)
    {
        $person = Person::where('id', $id)->first();
        if (!$person) {
            return Response::failApi(['message' => 'Data tidak ditemukan']);
        }

        $user = User::where('id', $id)->first()->setAppends([]);
        $student = Student::where('id', $id)->first();

        return Response::successApi([
            'data' => [
                'user' => $user,
                'person' => $person,
                'student' => $student
            ]
        ]);
    }

    public function edit($id, Request $request)
    {
        $validator = Validator($request->all(), [
            'person' => ['required', 'array'],
            'person.name' => ['required'],
            'student' => ['array'],
            'student.alias' => ['nullable', Rule::unique('students', 'alias')->where(function ($query) use ($id) {
                return $query->where('id', '!=', $id)->whereNotNull('alias');
            })],
            'student.nip' => ['nullable', Rule::unique('students', 'nip')->where(function ($query) use ($id) {
                return $query->where('id', '!=', $id)->whereNotNull('nip');
            })],
            'user' => ['array'],
            'user.username' => ['required', Rule::unique('users', 'username')->where(function ($query) use ($id) {
                return $query->where('id', '!=', $id);
            })],
            'user.password' => ['nullable', 'string', 'min:6'],
        ]);
        if ($validator->fails()) {
            return Response::failApi(['message' => $validator->errors()->first()]);
        }
        DB::beginTransaction();
        try {
            $person = Person::where('id', $id)->update($request->person);

            $student = $request->student;
            Student::where('id', $id)->update($student);

            $user = [
                'username' => $request->input('user.username'),
                'is_admin' => $request->has('user.is_admin'),
                'is_student' => $request->has('user.is_student'),
                'is_bk' => $request->has('user.is_bk'),
            ];
            if ($request->input('user.password', false)) {
                $user['password'] = Hash::make($request->input('user.password'));
            }
            User::where('id', $id)->update($user);

            DB::commit();
            return Response::successApi();
        } catch (\Throwable $th) {
            DB::rollBack();
            return Response::failApi(['message' => $th->getMessage()]);
        }
    }
}
