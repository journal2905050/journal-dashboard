<?php

namespace App\Http\Controllers\Modules;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Response;
use App\Models\Data\Employee;
use App\Models\Data\Person;
use App\Models\User;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;

class EmployeeController extends Controller
{
    function __construct()
    {
        $this->middleware('auth', ['except' => []]);
    }
    public function index()
    {
        return view('modules.employees.index');
    }

    public function get(Request $request)
    {
        $model = Person::join('employees', 'people.id', 'employees.id')
            ->select([
                'people.id',
                'people.name',
                'employees.nip',
                'employees.alias',
                'employees.rank',
                'employees.position',
                'employees.status',
            ]);

        return DataTables::of($model)
            ->addColumn('action', 'modules.employees.datatable.action')
            ->rawColumns(['action'])
            ->make(true);
    }

    public function store(Request $request)
    {
        $validator = Validator($request->all(), [
            'person' => ['required', 'array'],
            'person.name' => ['required'],
            'employee' => ['array'],
            'employee.alias' => ['nullable', Rule::unique('employees', 'alias')->whereNotNull('alias')],
            'employee.nip' => ['nullable', Rule::unique('employees', 'nip')->whereNotNull('nip')],
            'user' => ['array'],
            'user.username' => ['required', Rule::unique('users', 'username')],
            'user.password' => ['required', 'string', 'min:6'],
        ]);
        if ($validator->fails()) {
            return Response::failApi(['message' => $validator->errors()->first()]);
        }
        DB::beginTransaction();
        try {
            $person = Person::create($request->person);

            $employee = $request->employee;
            $employee['id'] = $person->id;
            Employee::create($employee);

            $user = [
                'id' => $person->id,
                'username' => $request->input('user.username'),
                'password' => $request->input('user.password'),
                'is_admin' => $request->has('user.is_admin'),
                'is_employee' => $request->has('user.is_employee'),
                'is_bk' => $request->has('user.is_bk'),
            ];
            User::create($user);

            DB::commit();
            return Response::successApi();
        } catch (\Throwable $th) {
            DB::rollBack();
            return Response::failApi();
        }
    }

    public function show($id)
    {
        $person = Person::where('id', $id)->first();
        if (!$person) {
            return Response::failApi(['message' => 'Data tidak ditemukan']);
        }

        $user = User::where('id', $id)->first()->setAppends([]);
        $employee = Employee::where('id', $id)->first();

        return Response::successApi([
            'data' => [
                'user' => $user,
                'person' => $person,
                'employee' => $employee
            ]
        ]);
    }

    public function edit($id, Request $request)
    {
        $validator = Validator($request->all(), [
            'person' => ['required', 'array'],
            'person.name' => ['required'],
            'employee' => ['array'],
            'employee.alias' => ['nullable', Rule::unique('employees', 'alias')->where(function ($query) use ($id) {
                return $query->where('id', '!=', $id)->whereNotNull('alias');
            })],
            'employee.nip' => ['nullable', Rule::unique('employees', 'nip')->where(function ($query) use ($id) {
                return $query->where('id', '!=', $id)->whereNotNull('nip');
            })],
            'user' => ['array'],
            'user.username' => ['required', Rule::unique('users', 'username')->where(function ($query) use ($id) {
                return $query->where('id', '!=', $id);
            })],
            'user.password' => ['nullable', 'string', 'min:6'],
        ]);
        if ($validator->fails()) {
            return Response::failApi(['message' => $validator->errors()->first()]);
        }
        DB::beginTransaction();
        try {
            $person = Person::where('id', $id)->update($request->person);

            $employee = $request->employee;
            Employee::where('id', $id)->update($employee);

            $user = [
                'username' => $request->input('user.username'),
                'is_admin' => $request->has('user.is_admin'),
                'is_employee' => $request->has('user.is_employee'),
                'is_bk' => $request->has('user.is_bk'),
            ];
            if ($request->input('user.password', false)) {
                $user['password'] = Hash::make($request->input('user.password'));
            }
            User::where('id', $id)->update($user);

            DB::commit();
            return Response::successApi();
        } catch (\Throwable $th) {
            DB::rollBack();
            return Response::failApi(['message' => $th->getMessage()]);
        }
    }
}
