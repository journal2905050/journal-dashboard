<?php

namespace App\Http\Controllers\Helper;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Services\DropdownService;
use App\Models\Data\AcademicPeriode;
use App\Models\Data\Classroom;
use App\Models\Data\JournalHour;
use App\Models\Data\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class DropdownController extends Controller
{
    public function gender()
    {
        return Response::successApi(['data' => DropdownService::gender()]);
    }
    public function grade()
    {
        return Response::successApi(['data' => DropdownService::grade()]);
    }
    public function school_year()
    {
        $ap = AcademicPeriode::select([
            'school_year',
            'is_active'
        ])->get();

        $ret = [];
        $selected = null;
        foreach ($ap as $key => $value) {
            $label = $value->school_year;
            if ($value->is_active) {
                $label = $value->school_year . ' [aktif]';
                $selected = $value->school_year;
            }
            $ret[] = [
                'key' => $value->school_year,
                'label' => $label
            ];
        }
        return Response::successApi([
            'data' => $ret,
            'meta' => ['selected' => $selected]
        ]);
    }
    public function classroom(Request $request)
    {
        $school_year = $request->input('school_year', '');
        $data = Classroom::where('school_year', $school_year);
        if ($request->input('grade', false)) {
            $data = $data->where('grade', $request->grade);
        }
        $data = $data->select([
            'id as key',
            'name as label'
        ])->orderBy('grade', 'ASC')->orderBy('name', 'asc')->get();

        return Response::successApi([
            'data' => $data
        ]);
    }

    public function batch_in_exist()
    {
        $data = Student::select([
            'batch_in as key',
            'batch_in as label'
        ])->orderBy('batch_in', 'desc')->distinct()->get();

        return Response::successApi(['data' => $data]);
    }

    public function journal_hour(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => ['required', 'date'],
            'classroom' => ['required', 'exists:classrooms,id']
        ], [
            'date.required' => 'Tanggal belum dipilih',
            'classroom.required' => 'Kelas belum dipilih'
        ]);
        if ($validator->fails()) {
            return Response::successApi([
                'data' => [],
                'meta' => [
                    'placeholder' => $validator->errors()->first()
                ]
            ]);
        }
        $ret = [];

        for ($i = 1; $i < 16; $i++) {
            $key = $i;
            $label = '[' . $i . ']';
            $is_disable = false;
            $exisit = JournalHour::join('journals', 'journal_hours.journal', 'journals.id')
                ->where([
                    'journals.classroom' => $request->classroom,
                    'journals.date' => $request->date,
                    'journal_hours.hour' => $i
                ])->select([
                    'journal_hours.hour',
                    'journals.employee',
                    'journals.subject',
                    'journals.id'
                ])->first();
            if ($exisit) {
                $label = '[' . $i . '] ' . $exisit->subject;
                $is_disable = true;
            }
            $ret[] = [
                'key' => $key,
                'label' => $label,
                'disable' => $is_disable
            ];
        }
        return Response::successApi([
            'data' => $ret
        ]);
    }
}
