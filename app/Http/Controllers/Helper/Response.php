<?php

namespace App\Http\Controllers\Helper;

use App\Http\Controllers\Controller;

class Response extends Controller
{
    public static function success($payload = [])
    {
        $payload = (object) $payload;
        return [
            'success' => $payload->success ?? true,
            'data' => $payload->data ?? null,
            'message' => $payload->message ?? 'Proses Berhasil.',
            'meta' => $payload->meta ?? []
        ];
    }

    public static function successApi($payload = [])
    {
        $payload = (object) $payload;
        return response()->json([
            'success' => $payload->success ?? true,
            'data' => $payload->data ?? null,
            'message' => $payload->message ?? 'Proses Berhasil.',
            'meta' => $payload->meta ?? []
        ], $payload->status_code ?? 200);
    }

    public static function fail($payload = [])
    {
        $payload = (object) $payload;
        return [
            'success' => $payload->success ?? false,
            'data' => $payload->data ?? null,
            'message' => $payload->message ?? 'Proses Gagal !!!!',
            'meta' => $payload->meta ?? []
        ];
    }
    public static function failApi($payload = [])
    {
        $payload = (object) $payload;
        return response()->json([
            'success' => $payload->success ?? false,
            'data' => $payload->data ?? null,
            'message' => $payload->message ?? 'Proses Gagal !!!!',
            'meta' => $payload->meta ?? []
        ], $payload->status_code ?? 200);
    }
}
