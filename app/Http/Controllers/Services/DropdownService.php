<?php

namespace App\Http\Controllers\Services;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DropdownService extends Controller
{
    public static function gender()
    {
        return collect([
            ['key' => 'L', 'label' => 'Laki-laki'],
            ['key' => 'P', 'label' => 'Perempuan'],
        ]);
    }

    public static function grade()
    {
        return collect([
            ['key' => '10', 'label' => '10'],
            ['key' => '11', 'label' => '11'],
            ['key' => '12', 'label' => '12'],
        ]);
    }
}
