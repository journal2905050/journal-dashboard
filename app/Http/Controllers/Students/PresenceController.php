<?php

namespace App\Http\Controllers\Students;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Response;
use App\Models\Data\PresenceDaily;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PresenceController extends Controller
{
    function __construct()
    {
        $this->middleware('auth', ['except' => []]);
    }
    public function index()
    {
        return view('students.presences.index');
    }

    public function get(Request $request)
    {
        $month = $request->input('month', Carbon::now()->format('Y-m'));
        $firstday = date('Y-m-d', strtotime($month . '-01'));
        $lastday = date('Y-m-t', strtotime($month . '-01'));

        $start = date_create($firstday);
        $end = date_create($lastday);
        $interval = new DateInterval('P1D');
        $date_range = new DatePeriod($start, $interval, $end);

        $user = Auth::user();

        $tbody = '';

        foreach ($date_range as $date) {
            $ret = PresenceDaily::where([
                'date' => $date,
                'student' => $user->id
            ])->select([
                'id',
                DB::raw('round(m) as m'),
                DB::raw('round(i) as i'),
                DB::raw('round(t) as t'),
                DB::raw('round(a) as a'),
                DB::raw('round(presentage) as presentage'),
            ])->first();
            if ($ret) {
                $tdsytle = '';
                if ($ret->presentage <= 50) {
                    $tdsytle = 'text-danger';
                }
                $tbody .= '<tr>';
                $tbody .= '<td>
                        <button id="btn-show-student-presence|' . $date->format('Y-m-d') . '"
                            type="button"
                            class="btn btn-icon btn-primary mr-1 mb-1 mt-2 btn-inline btn-show-presence">
                                <i class="feather icon-eye"></i>
                        </button>
                    </td>';
                $tbody .= '<td>' . $date->format("Y-m-d") . '</td>';
                $tbody .= '<td class="text-center">' . $ret->m ?? 0 . '</td>';
                $tbody .= '<td class="text-center">' . $ret->i ?? 0 . '</td>';
                $tbody .= '<td class="text-center">' . $ret->t ?? 0 . '</td>';
                $tbody .= '<td class="text-center">' . $ret->a ?? 0 . '</td>';
                $tbody .= '<td class="text-center ' . $tdsytle . '">' . $ret->presentage ?? ' - ' . ' %</td>';
                $tbody .= '<tr>';
            } else {
                $tbody .= '<tr class="table-dark">';
                $tbody .= '<td></td>';
                $tbody .= "<td>" . $date->format('Y-m-d') . "</td>";
                $tbody .= '<td class="text-center">-</td>';
                $tbody .= '<td class="text-center">-</td>';
                $tbody .= '<td class="text-center">-</td>';
                $tbody .= '<td class="text-center">-</td>';
                $tbody .= '<td class="text-center">-</td>';
                $tbody .= '<tr>';
            }
        }

        return Response::successApi([
            'data' => ['tbody' => $tbody]
        ]);
    }

    public function colorPresentage($presentage = null)
    {
        if ($presentage == null) {
            return 'dark';
        }
        $diff = 100 - $presentage;
    }
}
