<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\Response;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthenticationController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            if (Auth::user()->is_student) {
                return view(('students.dashboards.index'));
            }
            return view('modules.dasboards.index');
        }
        return view('auths.auth');
    }

    public function signIn(Request $request)
    {
        $payload = $request->only(['username', 'password']);
        $validator = Validator::make($payload, [
            'username' => 'required',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return view('auths.auth', ['message' => $validator->errors()->first()]);
        }
        if (Auth::attempt($payload)) {
            return redirect('');
        }
        return view('auths.auth', ['message' => 'Username atau password tidak sesuai.']);
    }

    public function signOut(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('');
    }

    public function changePassword(Request $request)
    {
        if (!Hash::check($request->password_exist, Auth::user()->password)) {
            return Response::failApi([
                'message' => 'Password saat ini tidak sesuai'
            ]);
        }
        User::where('id', Auth::user()->id)->update([
            'password' => Hash::make($request->password_new)
        ]);
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Response::success();
    }
}
