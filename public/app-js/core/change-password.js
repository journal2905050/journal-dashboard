$(document).ready(function () {
    $(document).on('submit', '#form-change-password', function (e) {
        e.preventDefault();

        var form = $('#form-change-password')[0];
        var data = new FormData(form);
        if (changePasswordKonfirmation()) {
            saveChangePassword(data);
            return false;
        } else {
            return false;
        }

    });
});


$("#btn-change-password").on("click", function () {
    $('#form-change-password')[0].reset();
    $("#span-change-password").html("Silakan lengkapi form ini");
    $('#modal-change-password').modal('show');
});

function saveChangePassword(data) {
    $.ajax({
        url: main_url + "/change-password",
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: data,
        success: function (ret) {
            if (ret.success) {
                $('#modal-change-password').modal('hide');
                location.reload();
            }
            if (!ret.success) {
                $("#span-change-password").html(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}

function changePasswordKonfirmation() {
    var newp = $('#cp-password-new').val();
    var confp = $('#cp-password-confirm').val();
    if (newp != confp) {
        $("#span-change-password").html("Konfirmasi password tidak sesuai");
        return false;
    }
    return true;
}
