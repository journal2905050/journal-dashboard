$(document).ready(function () {
    $(".date-now").val(dateNow());
    $(".month-now").val(monthNow());
});

function dateNow() {
    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '-' +
        (('' + month).length < 2 ? '0' : '') + month + '-' +
        (('' + day).length < 2 ? '0' : '') + day;
    return output;
}

function monthNow() {
    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '-' +
        (('' + month).length < 2 ? '0' : '') + month;
    return output;
}


