$(document).ready(function () {
    $(".select2").select2({
        dropdownAutoWidth: true,
        width: '100%'
    });
});

function setDropdownDefault(endpoint, multiple = false) {
    $.ajax({
        url: main_url + '/dropdown/' + endpoint,
        type: "get",
        dataType: "json",
        success: function (ret) {
            if (ret.success) {
                optHtml = `<option></option>`;
                jQuery.each(ret.data, function (i, val) {
                    var is_disable = '';
                    if (typeof val.disable !== 'undefined') {
                        if (val.disable) {
                            is_disable = 'disabled="disabled"'
                        }
                    }
                    optHtml += `<option value='` + val.key + `' ` + is_disable + `>` + val.label + `</option>`;
                });
                $('.select-' + endpoint).html(optHtml);
                $('.select-' + endpoint).select2({
                    placeholder: "-- Belum Dilpilih --",
                    allowClear: true,
                    width: '100%',
                    dropdownAutoWidth: true,
                    multiple: multiple,
                });
                if (typeof ret.meta.selected !== 'undefined') {
                    $('.select-' + endpoint).val(ret.meta.selected).change();
                }
            }
            if (!ret.success) {
                toastr.warning(ret.message, 'Set Dropdown', { "progressBar": true });
            }
        }
    });
}

function setDropdownById(id, endpoint, data = {}, multiple = false) {
    $.ajax({
        url: main_url + '/dropdown/' + endpoint,
        type: "get",
        data: data,
        dataType: "json",
        success: function (ret) {
            if (ret.success) {
                placeholds = '-- Belum Dilpilih --';
                if (typeof ret.meta.placeholder !== 'undefined') {
                    placeholds = ret.meta.placeholder;
                }
                optHtml = `<option></option>`;
                jQuery.each(ret.data, function (i, val) {
                    var is_disable = '';
                    if (typeof val.disable !== 'undefined') {
                        if (val.disable) {
                            is_disable = 'disabled="disabled"'
                        }
                    }
                    optHtml += `<option value='` + val.key + `' ` + is_disable + `>` + val.label + `</option>`;
                });
                $('#' + id).html(optHtml);
                $('#' + id).select2({
                    placeholder: "-- Belum Dilpilih --",
                    allowClear: true,
                    width: '100%',
                    dropdownAutoWidth: true,
                    multiple: multiple,
                });
                if (typeof ret.meta.selected !== 'undefined') {
                    $('#' + id).val(ret.meta.selected).change();
                }

            }
            if (!ret.success) {
                toastr.warning(ret.message, 'Set Dropdown', { "progressBar": true });
            }
        }
    });
}
