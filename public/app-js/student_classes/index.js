$(document).ready(function () {
    loadStudentClass();
    setDropdownDefault('school-year');
    setDropdownDefault('grade');
    setDropdownDefault('batch-in-exist');
});

function onSuccessProcessStudentClass() {
    loadStudentClass();
}

$('#student_class-select-school-year').on('change', function () {
    setStudentClassDdClassroomSearch();
    loadStudentClass();
});
$('#student_class-select-grade').on('change', function () {
    setStudentClassDdClassroomSearch();
    loadStudentClass();
});
$('#student_class-select-classroom').on('change', function () {
    loadStudentClass();
});

function setStudentClassDdClassroomSearch() {
    setDropdownById('student_class-select-classroom', 'classroom', {
        'school_year': $('#student_class-select-school-year').val(),
        'grade': $('#student_class-select-grade').val()
    });
}

function loadStudentClass() {
    $("#table-student_class").DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        scrollCollapse: true,
        scrollY: '350px',
        ajax: {
            'url': main_url + "/student-class/get",
            'data': {
                'school_year': $('#student_class-select-school-year').val(),
                'grade': $('#student_class-select-grade').val(),
                'classroom': $('#student_class-select-classroom').val()
            }
        },
        columns: [
            {
                data: "nis",
                name: "students.nis",
            },
            {
                data: "name",
                name: "people.name",
            },
            {
                data: "class_name",
                name: "classrooms.name",
            },
            {
                data: "grade",
                name: "classrooms.grade",
            },
            {
                data: "school_year",
                name: "classrooms.school_year",
            }
        ],
        order: [[2, 'desc']]
    });
}
