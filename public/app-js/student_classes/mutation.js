$(document).ready(function () {
    loadStudentMutation();
    $(document).on('submit', '#form-student-mutation', function (e) {
        e.preventDefault();

        var form = $('#form-student-mutation')[0];
        var data = new FormData(form);

        saveStudentMutation(data);
        return false;
    });
});


$('#btn-student_class-mutation').on('click', function () {
    show_modal_student_class_mutation();
});

function show_modal_student_class_mutation() {
    reset_modal_student_class_mutation();
    $('#modal-student_class-mutation').modal('show');
}

function close_modal_student_class_mutation() {
    reset_modal_student_class_mutation();
    $('#modal-student_class-mutation').modal('hide');
}

function reset_modal_student_class_mutation() {
    $(".form-select").val('').change();
}

$('#student_class-school_year-mutation').on('change', function () {
    setStudentClassDdClassroom();
});

$('#student_class-grade-mutation').on('change', function () {
    setStudentClassDdClassroom();
});

$('#student_class-school_year-mutation').on('change', function () {
    loadStudentMutation();
});
$('#student_class-classroom-mutation').on('change', function () {
    loadStudentMutation();
});
$('#student_class-batch-in-mutation-search').on('change', function () {
    loadStudentMutation();
});
$('#student_class-classless-mutation-search').on('change', function () {
    loadStudentMutation();
});

function setStudentClassDdClassroom() {
    setDropdownById('student_class-classroom-mutation', 'classroom', {
        'school_year': $('#student_class-school_year-mutation').val(),
        'grade': $('#student_class-grade-mutation').val()
    });
}


function saveStudentMutation(data) {
    $.ajax({
        url: main_url + "/student-mutation/add",
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: data,
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'ADD  Classroom', { "progressBar": true });
                loadStudentMutation();
                onSuccessProcessStudentClass();
            }
            if (!ret.success) {
                toastr.warning(ret.message, 'ADD  Classroom', { "progressBar": true });
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}

function loadStudentMutation() {
    $("#table-student-mutation").DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        paging: false,
        scrollCollapse: true,
        scrollY: '300px',
        ajax: {
            'url': main_url + "/student-mutation",
            'data': {
                'school_year': $('#student_class-school_year-mutation').val(),
                'classroom': $('#student_class-classroom-mutation').val(),
                'batch_in': $('#student_class-batch-in-mutation-search').val(),
                'classless': $('#student_class-classless-mutation-search').is(":checked")
            }
        },
        columns: [
            {
                data: "action",
                name: "students.id",
            },
            {
                data: "nis",
                name: "students.nis",
            },
            {
                data: "name",
                name: "people.name",
            },
            {
                data: "class_name",
                name: "classrooms.name",
            }
        ],
        order: [[0, 'desc']]
    });
}




