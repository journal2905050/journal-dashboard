$(document).ready(function () {
    $(document).on('submit', '#form-classroom-add', function (e) {
        e.preventDefault();

        var form = $('#form-classroom-add')[0];
        var data = new FormData(form);

        saveClassroomAdd(data);
        return false;
    });
});


$('#btn-classroom-add').on('click', function () {
    show_modal_classroom_add();
});

function show_modal_classroom_add() {
    reset_modal_classroom_add();
    $('#modal-classroom-add').modal('show');
}

function close_modal_classroom_add() {
    reset_modal_classroom_add();
    $('#modal-classroom-add').modal('hide');
}

function reset_modal_classroom_add() {
    $('#form-classroom-add')[0].reset();
    $(".form-select").val('').change();
}


function saveClassroomAdd(data) {
    $.ajax({
        url: main_url + "/classroom/add",
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: data,
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'ADD  Classroom', { "progressBar": true });
                close_modal_classroom_add();
                onSuccessProcessClassroom();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'ADD  Classroom', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}


