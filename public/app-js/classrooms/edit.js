let id_classroom_form;
$(document).ready(function () {
    $(document).on('submit', '#form-classroom-edit', function (e) {
        e.preventDefault();

        var form = $('#form-classroom-edit')[0];
        var data = new FormData(form);

        saveClassroomEdit(data);
        return false;
    });
});

$('#form-classroom-remove').on('click', function () {
    deleteClassroomEdit();
});

function show_modal_classroom_edit(id) {
    id_classroom_form = id;
    reset_modal_classroom_edit();
    $('#modal-classroom-edit').modal('show');
}

function close_modal_classroom_edit() {
    reset_modal_classroom_edit();
    $(".form-select").val('').change();
    $('#modal-classroom-edit').modal('hide');
}

function reset_modal_classroom_edit() {
    $('#form-classroom-edit')[0].reset();
    $.ajax({
        url: main_url + "/classroom/show/" + id_classroom_form,
        type: "GET",
        processData: false,
        contentType: false,
        cache: false,
        data: { '_token': _token },
        success: function (ret) {
            if (ret.success) {
                var rd = ret.data;
                $('#classroom-name-edit').val(rd.name);
                $('#classroom-school_year-edit').val(rd.school_year).change();
                $('#classroom-grade-edit').val(rd.grade).change();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'Show  Classroom', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}




function saveClassroomEdit(data) {
    $.ajax({
        url: main_url + "/classroom/edit/" + id_classroom_form,
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: data,
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'Edit  Classroom', { "progressBar": true });
                close_modal_classroom_edit();
                onSuccessProcessClassroom();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'Edit  Classroom', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}



function deleteClassroomEdit() {
    $.ajax({
        url: main_url + "/classroom/delete/" + id_classroom_form,
        type: "DELETE",
        cache: false,
        data: {
            '_token': _token
        },
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'Hapus  Classroom', { "progressBar": true });
                close_modal_classroom_edit();
                onSuccessProcessClassroom();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'Hapus  Classroom', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}


