$(document).ready(function () {
    loadClassroom();
    setDropdownDefault('school-year');
    setDropdownDefault('grade');
});

function onSuccessProcessClassroom() {
    loadClassroom();
}

$('#classroom-select-school-year').on('change', function () {
    loadClassroom();
});
$('#classroom-select-grade').on('change', function () {
    loadClassroom();
});

function loadClassroom() {
    $("#table-classroom").DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        scrollCollapse: true,
        scrollY: '350px',
        ajax: {
            'url': main_url + "/classroom/get",
            'data': {
                'school_year': $('#classroom-select-school-year').val(),
                'grade': $('#classroom-select-grade').val()
            }
        },
        columns: [
            {
                data: "action",
                name: "id",
            },
            {
                data: "school_year",
                name: "school_year",
            },
            {
                data: "grade",
                name: "grade",
            },
            {
                data: "name",
                name: "name",
            }
        ],
        order: [[1, 'desc'], [2, 'asc'], [3, 'asc']]
    });
}
