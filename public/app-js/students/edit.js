let id_student_form;
$(document).ready(function () {
    $(document).on('submit', '#form-student-edit', function (e) {
        e.preventDefault();

        var form = $('#form-student-edit')[0];
        var data = new FormData(form);

        saveStudentEdit(data);
        return false;
    });
});


function show_modal_student_edit(id) {
    id_student_form = id;
    reset_modal_student_edit();
    $('#modal-student-edit').modal('show');
}

function close_modal_student_edit() {
    reset_modal_student_edit();
    $(".form-select").val('').change();
    $('#modal-student-edit').modal('hide');
}

function reset_modal_student_edit() {
    $('#form-student-edit')[0].reset();
    $.ajax({
        url: main_url + "/student/show/" + id_student_form,
        type: "GET",
        processData: false,
        contentType: false,
        cache: false,
        data: { '_token': _token },
        success: function (ret) {
            if (ret.success) {
                var person = ret.data.person;
                var student = ret.data.student;
                var user = ret.data.user;
                $('#student-name-edit').val(person.name);
                $('#student-gender-edit').val(person.gender).change();
                $('#student-nis-edit').val(student.nis);
                $('#student-batch_in-edit').val(student.batch_in);
                $('#student-username-edit').val(user.username);
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'Show  Student', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}




function saveStudentEdit(data) {
    $.ajax({
        url: main_url + "/student/edit/" + id_student_form,
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: data,
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'ADD  Student', { "progressBar": true });
                close_modal_student_edit();
                onSuccessProcessStudent();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'ADD  Student', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}


