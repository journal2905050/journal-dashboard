$(document).ready(function () {
    $(document).on('submit', '#form-student-add', function (e) {
        e.preventDefault();

        var form = $('#form-student-add')[0];
        var data = new FormData(form);

        saveStudentAdd(data);
        return false;
    });
});


$('#btn-student-add').on('click', function () {
    show_modal_student_add();
});

function show_modal_student_add() {
    reset_modal_student_add();
    $('#modal-student-add').modal('show');
}

function close_modal_student_add() {
    reset_modal_student_add();
    $('#modal-student-add').modal('hide');
}

function reset_modal_student_add() {
    $('#form-student-add')[0].reset();
    $(".form-select").val('').change();
}


function saveStudentAdd(data) {
    $.ajax({
        url: main_url + "/student/add",
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: data,
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'ADD  Student', { "progressBar": true });
                close_modal_student_add();
                onSuccessProcessStudent();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'ADD  Student', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}


