$(document).ready(function () {
    loadStudent();
    setDropdownDefault('gender');
});

function onSuccessProcessStudent() {
    loadStudent();
}

function loadStudent() {
    $("#table-student").DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            'url': main_url + "/student/get",
        },
        columns: [
            {
                data: "action",
                name: "id",
            },
            {
                data: "nis",
                name: "students.nis",
            },
            {
                data: "name",
                name: "name",
            },
            {
                data: "gender",
                name: "gender",
            },
            {
                data: "batch_in",
                name: "students.batch_in",
            },
        ],
        order: [[4, 'desc']]
    });
}
