$(document).ready(function () {
    loadPeriode();
});

function loadPeriode() {
    $("#table-periode").DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            'url': main_url + "/periode/get",
        },
        columns: [
            {
                data: "school_year",
                name: "school_year",
            },
            {
                data: "set_active",
                name: "is_active",
            },
            {
                data: "action",
                name: "id",
            }
        ],
        order: [[0, 'desc']]
    });
}

function onSuccessProcessPeriode() {
    loadPeriode();
}

function setActivePeriode(id) {
    $.ajax({
        url: main_url + "/periode/activation/" + id,
        type: "POST",
        cache: false,
        data: { '_token': _token },
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'Activation Periode', { "progressBar": true });
                onSuccessProcessPeriode();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'Activation Periode', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}
function deletePeriode(id) {
    $.ajax({
        url: main_url + "/periode/" + id,
        type: "DELETE",
        cache: false,
        data: { '_token': _token },
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'Delete Periode', { "progressBar": true });
                onSuccessProcessPeriode();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'Delete Periode', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}


