$(document).ready(function () {
    $(document).on('submit', '#form-periode-add', function (e) {
        e.preventDefault();

        var form = $('#form-periode-add')[0];
        var data = new FormData(form);

        savePeriodeAdd(data);
        return false;
    });
});

$('#btn-periode-add').on('click', function () {
    show_modal_periode_add();
});

function show_modal_periode_add() {
    reset_modal_periode_add();
    $('#modal-periode-add').modal('show');
}

function close_modal_periode_add() {
    reset_modal_periode_add();
    $('#modal-periode-add').modal('hide');
}

function reset_modal_periode_add() {
    $('#form-periode-add')[0].reset();
}

function savePeriodeAdd(data) {
    $.ajax({
        url: main_url + "/periode/add",
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: data,
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'ADD  Periode', { "progressBar": true });
                close_modal_periode_add();
                onSuccessProcessPeriode();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'ADD  Periode', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}
