let id_student_presence = presence_student_id;
$(document).ready(function () {
    console.log(presence_student_id);
});


function show_modal_presence_edit(payload) {
    var id = payload.id.split('|');
    var date = id[1];
    reset_modal_presence_edit(date);
}
function close_modal_presence_edit() {
    $('#form-student-presence-edit')[0].reset();
    $('#modal-student-presence-edit').modal('hide');
}

function reset_modal_presence_edit(date) {
    $('#form-student-presence-edit')[0].reset();
    $('#presence-date-edit').val(date);
    $.ajax({
        url: main_url + "/bk-presence/show/" + id_student_presence,
        type: "GET",
        // processData: false,
        // contentType: false,
        cache: false,
        data: { '_token': _token, 'date': date },
        success: function (ret) {
            if (ret.success) {
                var rd = ret.data;
                $('#presence-nis-edit').val(rd.student.nis)
                $('#presence-name-edit').val(rd.student.name);
                $('#tbody-student-presence-show').html(rd.tbody);
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'Set up presensi', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        },
        complete: function () {
            $('#modal-student-presence-edit').modal('show');
        }
    });
}
