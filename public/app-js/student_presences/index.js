$(document).ready(function () {
    load_student_presence();
});

$('#student-presence-month').on('change', function () {
    load_student_presence();
});

function load_student_presence() {
    month = $('#student-presence-month').val();
    $.ajax({
        url: main_url + "/student-presence/get",
        type: "GET",
        cache: false,
        data: {
            "_token": _token,
            "month": month
        },
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'Load tabel presensi', { "progressBar": true });
                $('#tbody-student-presence').html(ret.data.tbody);
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'Load tabel presensi', { "progressBar": true });
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        },
        complete: function () {
            $(".btn-show-presence").on("click", function () {
                show_modal_presence_edit(this);
            });
        }
    });
}
