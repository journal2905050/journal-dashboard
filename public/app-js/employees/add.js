$(document).ready(function () {
    $(document).on('submit', '#form-employee-add', function (e) {
        e.preventDefault();

        var form = $('#form-employee-add')[0];
        var data = new FormData(form);

        saveEmployeeAdd(data);
        return false;
    });
});


$('#btn-employee-add').on('click', function () {
    show_modal_employee_add();
});

function show_modal_employee_add() {
    reset_modal_employee_add();
    $('#modal-employee-add').modal('show');
}

function close_modal_employee_add() {
    reset_modal_employee_add();
    $('#modal-employee-add').modal('hide');
}

function reset_modal_employee_add() {
    $('#form-employee-add')[0].reset();
}


function saveEmployeeAdd(data) {
    $.ajax({
        url: main_url + "/employee/add",
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: data,
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'ADD  Employee', { "progressBar": true });
                close_modal_employee_add();
                onSuccessProcessEmployee();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'ADD  Employee', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}


