$(document).ready(function () {
    loadEmployee();
});

function onSuccessProcessEmployee() {
    loadEmployee();
}

function loadEmployee() {
    $("#table-employee").DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            'url': main_url + "/employee/get",
        },
        columns: [
            {
                data: "action",
                name: "id",
            },
            {
                data: "nip",
                name: "employees.nip",
            },
            {
                data: "name",
                name: "name",
            },
            {
                data: "alias",
                name: "employees.alias",
            },
            {
                data: "rank",
                name: "employees.rank",
            },
            {
                data: "position",
                name: "employees.position",
            },
            {
                data: "status",
                name: "employees.status",
            }
        ],
        order: [[1, 'desc']]
    });
}
