let id_employee_form;
$(document).ready(function () {
    $(document).on('submit', '#form-employee-edit', function (e) {
        e.preventDefault();

        var form = $('#form-employee-edit')[0];
        var data = new FormData(form);

        saveEmployeeEdit(data);
        return false;
    });
});


function show_modal_employee_edit(id) {
    id_employee_form = id;
    reset_modal_employee_edit();
    $('#modal-employee-edit').modal('show');
}

function close_modal_employee_edit() {
    reset_modal_employee_edit();
    $('#modal-employee-edit').modal('hide');
}

function reset_modal_employee_edit() {
    $('#form-employee-edit')[0].reset();
    $.ajax({
        url: main_url + "/employee/show/" + id_employee_form,
        type: "GET",
        processData: false,
        contentType: false,
        cache: false,
        data: { '_token': _token },
        success: function (ret) {
            if (ret.success) {
                var person = ret.data.person;
                var employee = ret.data.employee;
                var user = ret.data.user;
                $('#employee-name-edit').val(person.name);
                $('#employee-alias-edit').val(employee.alias);
                $('#employee-nip-edit').val(employee.nip);
                $('#employee-position-edit').val(employee.position);
                $('#employee-rank-edit').val(employee.rank);
                $('#employee-status-edit').val(employee.status);
                $('#employee-username-edit').val(user.username);
                document.getElementById('employee-is_admin-edit').checked = user.is_admin;
                document.getElementById('employee-is_employee-edit').checked = user.is_employee;
                document.getElementById('employee-is_bk-edit').checked = user.is_bk;
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'Show  Employee', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}




function saveEmployeeEdit(data) {
    $.ajax({
        url: main_url + "/employee/edit/" + id_employee_form,
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: data,
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'ADD  Employee', { "progressBar": true });
                close_modal_employee_edit();
                onSuccessProcessEmployee();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'ADD  Employee', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}


