let id_employee_journal_form;
$(document).ready(function () {
    $(document).on('submit', '#form-employee_journal-edit', function (e) {
        e.preventDefault();

        var date = $('#date-employee_journal-edit').val();
        var summary = $("#summary-employee_journal-edit").val();
        var presences = [];

        $(".div-presence").each(function (index) {
            // console.log(this.id);
            var thisid = this.id.split('|');
            var type = $(this).text();
            if (type == 'X') {
                type = null;
            }
            presences.push({
                "journal": id_employee_journal_form,
                "student": thisid[0],
                "date": date,
                "hour": thisid[1],
                "type": type
            });
        });

        var data = {
            "summary": summary,
            "presences": presences,
            '_token': _token,
            'classroom': $('#employee_journal-select-classroom-edit').val(),
            'date': date
        };
        saveEmployeeJournalEdit(data);
        return false;
    });

});


function saveEmployeeJournalEdit(data) {
    $.ajax({
        url: main_url + "/employee-journal/presence/" + id_employee_journal_form,
        type: "POST",
        // enctype: 'multipart/form-data',
        // processData: false,
        // contentType: false,
        // cache: false,
        data: data,
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'ADD  Employee Journal', { "progressBar": true });
                close_modal_employee_journal_edit();
                onSuccessProcessEmployeeJournal();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'ADD  Employee Journal', { "progressBar": false });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}


function show_modal_employee_journal_edit(id) {
    id_employee_journal_form = id;
    reset_modal_employee_journal_edit();
    $('#modal-employee_journal-edit').modal('show');
}

function close_modal_employee_journal_edit() {
    reset_modal_employee_journal_edit();
    $(".form-select").val('');
    $('#modal-employee_journal-edit').modal('hide');
}


function reset_modal_employee_journal_edit() {
    $('#form-employee_journal-edit')[0].reset();
    $.ajax({
        url: main_url + "/employee-journal/show/" + id_employee_journal_form,
        type: "GET",
        processData: false,
        contentType: false,
        cache: false,
        data: { '_token': _token },
        success: function (ret) {
            if (ret.success) {
                var rd = ret.data;
                $('#date-employee_journal-edit').val(rd.date);
                $('#school-year-employee_journal-edit').val(rd.school_year);
                $('#grade-employee_journal-edit').val(rd.grade);
                $('#employee_journal-select-classroom-edit').val(rd.class_name);
                $('#employee_journal-select-subject-edit').val(rd.subject);
                $('#hour-employee_journal-edit').val(rd.hours_label);
                $('#summary-employee_journal-edit').html(rd.summary);

                $('#thead-employee_journal').html(rd.table.thead);
                $('#tbody-employee_journal').html(rd.table.tbody);
                // $('#table-employee_journal-edit').css({
                //     'height': '400px',
                //     'overflow-y': 'auto',
                //     'overflow-x': 'hidden'
                // });

            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'Show  Classroom', { "progressBar": true });

            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        },
        complete: function () {
            $(".div-presence").on("click", function () {
                // console.log($(this).html());
                changeDivPresence(this);
            });
            // $(".div-presence").css('background-color', 'yellow');
            setDivPresence();
        }
    });
}

function setDivPresence() {
    $(".div-presence").each(function (index) {
        var thtm = $(this).text();
        switch (thtm) {
            case 'X':
                $(this).css({ 'background-color': '#b8c2cc' });
                break;
            case 'M':
                $(this).css({ 'background-color': '#28c76f' });
                break;
            case 'I':
                $(this).css({ 'background-color': '#00cfe8' });
                break;
            case 'T':
                $(this).css({ 'background-color': '#ff9f43' });
                break;
            case 'A':
                $(this).css({ 'background-color': '#ea5455' });
                break;

            default:
                break;
        }
    });
}

function changeDivPresence(payload) {
    var thtm = $(payload).html();
    switch (thtm) {
        case 'X':
            $(payload).text('M');
            break;
        case 'M':
            $(payload).text('I');
            break;
        case 'I':
            $(payload).text('T');
            break;
        case 'T':
            $(payload).text('A');
            break;
        case 'A':
            $(payload).text('X');
            break;

        default:
            break;
    }
    setDivPresence();
}



