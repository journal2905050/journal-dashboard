$(document).ready(function () {
    setDropdownDefault('school-year');
    setDropdownDefault('grade');
    loadEmployeeJournal();
});

$('#employee_journal-select-date').on('change', function () {
    loadEmployeeJournal();
});

$('#employee_journal-select-school-year').on('change', function () {
    setEmployeeJournalDdClassroom();
});

$('#employee_journal-select-grade').on('change', function () {
    setEmployeeJournalDdClassroom();
});

function setEmployeeJournalDdClassroom() {
    setDropdownById('employee_journal-select-classroom', 'classroom', {
        'school_year': $('#employee_journal-select-school-year').val(),
        'grade': $('#employee_journal-select-grade').val()
    });
}

function onSuccessProcessEmployeeJournal() {
    loadEmployeeJournal();
}

function loadEmployeeJournal() {
    $("#table-employee_journal").DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            'url': main_url + "/employee-journal/get",
            'data': {
                'date': $('#employee_journal-select-date').val()
            }
        },
        columns: [
            {
                data: "class_name",
                name: "classrooms.name",
            },
            {
                data: "grade",
                name: "classrooms.grade",
            },
            {
                data: "subject",
                name: "subject",
            },
            {
                data: "hours",
                searchable: false,
                ordering: false,
            },
            {
                data: "action",
                name: "id",
            }
        ],
        order: [[3, 'asc']]
    });
}
