$(document).ready(function () {
    setDropdownDefault('school-year');
    setDropdownDefault('grade');

    $(document).on('submit', '#form-employee_journal-add', function (e) {
        e.preventDefault();

        var form = $('#form-employee_journal-add')[0];
        var data = new FormData(form);

        saveEmployeeJournalAdd(data);
        return false;
    });
});

$('#btn-employee_journal-add').on('click', function () {
    show_modal_employee_journal_add();
});


function show_modal_employee_journal_add() {
    reset_modal_employee_journal_add();
    $('#modal-employee_journal-add').modal('show');
}

function close_modal_employee_journal_add() {
    reset_modal_employee_journal_add();
    $('#modal-employee_journal-add').modal('hide');
}

function reset_modal_employee_journal_add() {
    $('#form-employee_journal-add')[0].reset();
    $(".form-select").val('').change();
    $('#date-employee_journal-add').val(dateNow());
    setEmployeeJournalDdHour();
}


$('#date-employee_journal-add').on('change', function () {
    setEmployeeJournalDdHour();
});

$('#employee_journal-select-classroom-add').on('change', function () {
    setEmployeeJournalDdHour();
});

$('#school-year-employee_journal-add').on('change', function () {
    setEmployeeJournalDdEmployeeJournal();
});

$('#grade-employee_journal-add').on('change', function () {
    setEmployeeJournalDdEmployeeJournal();
});

function setEmployeeJournalDdHour() {
    setDropdownById('hour-employee_journal-add', 'journal-hour', {
        'classroom': $('#employee_journal-select-classroom-add').val(),
        'date': $('#date-employee_journal-add').val()
    }, true);
}

function setEmployeeJournalDdEmployeeJournal() {
    setDropdownById('employee_journal-select-classroom-add', 'classroom', {
        'school_year': $('#school-year-employee_journal-add').val(),
        'grade': $('#grade-employee_journal-add').val()
    });
}

function saveEmployeeJournalAdd(data) {
    $.ajax({
        url: main_url + "/employee-journal/add",
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: data,
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'ADD  Employee Journal', { "progressBar": true });
                close_modal_employee_journal_add();
                onSuccessProcessEmployeeJournal();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'ADD  Employee Journal', { "progressBar": false });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}
