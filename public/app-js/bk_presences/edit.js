let id_student_presence;
$(document).ready(function () {
    $(document).on('submit', '#form-presence-edit', function (e) {
        e.preventDefault();

        var form = $('#form-presence-edit')[0];
        var data = new FormData(form);

        savePresenceEdit(data);
        return false;
    });
});


function show_modal_presence_edit(payload) {
    var id = payload.id.split('|');
    var student = id[1];
    id_student_presence = student;
    reset_modal_presence_edit();
}
function close_modal_presence_edit(payload) {
    $('#form-presence-edit')[0].reset();
    $('#modal-presence-edit').modal('hide');
}

function reset_modal_presence_edit() {
    $('#form-presence-edit')[0].reset();
    var date = $("#bk_presence-select-date").val();
    $('#presence-date-edit').val(date);
    $.ajax({
        url: main_url + "/bk-presence/show/" + id_student_presence,
        type: "GET",
        // processData: false,
        // contentType: false,
        cache: false,
        data: { '_token': _token, 'date': date },
        success: function (ret) {
            if (ret.success) {
                var rd = ret.data;
                $('#presence-nis-edit').val(rd.student.nis)
                $('#presence-name-edit').val(rd.student.name);
                $('#tbody-bk_presence-show').html(rd.tbody);
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'Set up presensi', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        },
        complete: function () {
            $('#modal-presence-edit').modal('show');
        }
    });
}


function savePresenceEdit(data) {
    $.ajax({
        url: main_url + "/bk-presence/edit/" + id_student_presence,
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        cache: false,
        data: data,
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'Edit  Presence', { "progressBar": true });
                console.log(ret);
                close_modal_presence_edit();
                onSuccessProcessPresence();
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'Edit  Presence', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        }
    });
}
