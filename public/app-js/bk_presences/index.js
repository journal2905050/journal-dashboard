$(document).ready(function () {
    setDropdownDefault('school-year');
    setDropdownDefault('grade');
    setDropdownDefault('batch-in-exist');
    loadPersenceTable();
});


$('#bk_presence-select-school-year').on('change', function () {
    setStudentClassDdClassroomSearch();
});
$('#bk_presence-select-grade').on('change', function () {
    setStudentClassDdClassroomSearch();
});
$('#bk_presence-select-classroom').on('change', function () {
    loadPersenceTable();
});
$('#bk_presence-select-date').on('change', function () {
    loadPersenceTable();
});

function onSuccessProcessPresence() {
    loadPersenceTable();
}

function setStudentClassDdClassroomSearch() {
    setDropdownById('bk_presence-select-classroom', 'classroom', {
        'school_year': $('#bk_presence-select-school-year').val(),
        'grade': $('#bk_presence-select-grade').val()
    });
}

function loadPersenceTable(data) {
    var date = $("#bk_presence-select-date").val();
    var classroom = $("#bk_presence-select-classroom").val();
    $.ajax({
        url: main_url + "/bk-presence/get",
        type: "GET",
        // enctype: 'multipart/form-data',
        // processData: false,
        // contentType: false,
        cache: false,
        data: {
            "_token": _token,
            "classroom": classroom,
            "date": date
        },
        success: function (ret) {
            if (ret.success) {
                toastr.success(ret.message, 'Load tabel presensi', { "progressBar": true });
                $("#tbody-bk_presence").html(ret.data.tbody);
            }
            if (!ret.success) {
                console.log(ret);
                toastr.warning(ret.message, 'Load tabel presensi', { "progressBar": true });

                console.log(ret.message);
            }
        },
        error: function (e) {
            toastr.warning('Some thing went wrong !!!', { "progressBar": true });
        },
        complete: function () {
            $(".btn-show-presence").on("click", function () {
                show_modal_presence_edit(this);
            });
            setDivPresence();
        }
    });
}


function setDivPresence() {
    $(".div-presence").each(function (index) {
        var thtm = $(this).text();
        switch (thtm) {
            case 'X':
                $(this).css({ 'background-color': '#b8c2cc' });
                break;
            case 'M':
                $(this).css({ 'background-color': '#28c76f' });
                break;
            case 'I':
                $(this).css({ 'background-color': '#00cfe8' });
                break;
            case 'T':
                $(this).css({ 'background-color': '#ff9f43' });
                break;
            case 'A':
                $(this).css({ 'background-color': '#ea5455' });
                break;

            default:
                break;
        }
    });
}

