<div class="vs-checkbox-con vs-checkbox-primary @if ($class ?? false) {{ $class }} @endif">
    <input type="checkbox" id="{{ $id }}" @if ($name ?? false) name="{{ $name }}" @endif
        @if ($checked ?? false) checked @endif value="{{ $value ?? false }}"
        @if ($onchange ?? false) onchange="{{ $onchange }}" @endif>
    <span class="vs-checkbox vs-checkbox-lg">
        <span class="vs-checkbox--check">
            <i class="vs-icon feather icon-check"></i>
        </span>
    </span>
    <span class="">{{ $label ?? '' }}</span>
</div>
