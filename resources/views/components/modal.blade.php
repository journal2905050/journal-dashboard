<div class="modal fade text-left" id="{{ $id }}" tabindex="-1" material="dialog"
    aria-labelledby="my-{{ $id }}" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-scrollable modal-{{ $size ?? 'lg' }}" material="document">
        <div class="modal-content">
            @if ($form ?? false)
                <form name="{{ $form['name'] ?? '' }}" id="{{ $form['id'] }}" method="{{ $method ?? 'POST' }}"
                    @if ($enctype ?? false) enctype='multipart/form-data' @endif>
                    @csrf
            @endif
            <div class="modal-header bg-primary white">
                <h4 class="modal-title" id="my-{{ $id }}">{{ $title }}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" @if ($styelBody ?? false) style="{{ $styleBody }}" @endif>
                {{ $slot }}
            </div>
            @if ($form ?? false)
                <hr class="divider">
                <div class="mt-2 mb-1 ml-2 mr-2">
                    <div class="row justify-content-between">
                        <div class="col-2">
                            @if ($remove ?? false)
                                <button type="button" class="btn btn-danger" id="{{ $remove['id'] }}">Hapus</button>
                            @endif
                        </div>
                        <div class="col-8 text-right">
                            <button type="submit" class="btn btn-success">Simpan</button>
                            <button type="reset" class="btn btn-warning" data-dismiss="modal">Batal</button>
                        </div>
                    </div>

                </div>

                </form>
            @endif
        </div>
    </div>
</div>

@push('custom-js')
    <script src="{{ asset('app-js/materials/add.js') }}"></script>
@endpush
