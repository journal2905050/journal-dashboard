<fieldset class="form-group">
    <label for="{{ $id }}">{{ $label }}</label>
    <select @if ($name ?? false) name="{{ $name }}" @endif id="{{ $id }}"
        class="form-control
        @if ($class ?? false) {{ $class }} @endif"
        @if ($required ?? false) required @endif
        @if ($multiple ?? false) multiple="multiple" @endif>{{-- Jika multiple dari JS pakai di Js aja, kalo langsung set opt baru di true multiplenya --}}
        @if ($opt ?? false)
            @foreach ($opt ?? [] as $item)
                <option value="{{ $item['key'] }}">{{ $item['label'] }}</option>
            @endforeach
        @endif
    </select>
</fieldset>
