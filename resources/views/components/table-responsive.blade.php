<div class="table-responsive">
    <table class="table" id="{{ $id }}">
        <thead>
            <tr>
                @foreach ($columns as $key => $value)
                    <th>{{ $value }}</th>
                @endforeach
            </tr>
        </thead>
        {{ $slot }}
    </table>
</div>
