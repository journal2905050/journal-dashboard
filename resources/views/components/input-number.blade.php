<fieldset class="form-group">
    <label for="{{ $id }}">{{ $label }}</label>
    <input type="number" id="{{ $id }}" name="{{ $name }}"
        @if ($min ?? false) min="{{ $min }}" @endif
        @if ($max ?? false) max="{{ $max }}" @endif
        class="form-control @if ($class ?? false) {{ $class }} @endif"
        placeholder="{{ $placeholder ?? $label }}" @if ($required ?? false) required @endif>
    @if ($span ?? false)
        <span class="text-{{ $span['color'] ?? 'danger' }}">{{ $span['label'] }}</span>
    @endif
</fieldset>
