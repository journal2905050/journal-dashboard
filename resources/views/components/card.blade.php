<section class="">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if ($title ?? false)
                    <div class="card-header">
                        <h4 class="card-title">{{ $title ?? '' }}</h4>
                    </div>
                @endif
                <div class="card-content">
                    <div class="card-body">
                        {{ $slot }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
