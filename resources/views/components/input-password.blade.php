<fieldset class="form-group">
    <label for="{{ $id }}">{{ $label }}</label>
    <input type="password" id="{{ $id }}" @if ($name ?? false) name="{{ $name }}" @endif
        class="form-control @if ($class ?? false) {{ $class }} @endif"
        placeholder="{{ $placeholder ?? $label }}" @if ($required ?? false) required @endif
        autocomplete="{{ $autocomplete ?? 'on' }}" @if ($readonly ?? false) readonly @endif>
    @if ($span ?? false)
        <span class="text-{{ $span['color'] ?? 'danger' }}">{{ $span['label'] }}</span>
    @endif
</fieldset>
