<fieldset class="form-group">
    <label for="{{ $id }}">{{ $label }}</label>
    <textarea id="{{ $id }}" name="{{ $name }}"
        class="form-control @if ($class ?? false) {{ $class }} @endif"
        placeholder="{{ $placeholder ?? $label }}" @if ($required ?? false) required @endif
        autocomplete="{{ $autocomplete ?? 'on' }}">
@if ($value ?? false)
{{ $value }}
@endif
</textarea>
    @if ($span ?? false)
        <span class="text-{{ $span['color'] ?? 'danger' }}">{{ $span['label'] }}</span>
    @endif
</fieldset>
