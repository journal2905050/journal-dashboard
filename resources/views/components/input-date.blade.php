<fieldset class="form-group">
    <label for="{{ $id }}">{{ $label }}</label>
    <input type="date" id="{{ $id }}" @if ($name ?? false) name="{{ $name }}" @endif
        @if ($min ?? false) min="{{ $min }}" @endif
        @if ($max ?? false) max="{{ $max }}" @endif
        class="form-control @if ($class ?? false) {{ $class }} @endif"
        placeholder="{{ $placeholder ?? $label }}" @if ($required ?? false) required @endif
        @if ($readonly ?? false) readonly @endif>
    @if ($span ?? false)
        <span class="text-{{ $span['color'] ?? 'danger' }}">{{ $span['label'] }}</span>
    @endif
</fieldset>
