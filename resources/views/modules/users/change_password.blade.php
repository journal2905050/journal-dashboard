@component('components.modal', [
    'id' => 'modal-change-password',
    'title' => 'Ganti Password',
    'size' => 'sm',
    'form' => [
        'id' => 'form-change-password',
        'name' => 'form-change-password',
        'method' => 'POST',
    ],
])
    <div class="form-body">
        <div class="row">
            <div class="col-12">
                @component('components.input-password', [
                    'id' => 'cp-password-exist',
                    'name' => 'password_exist',
                    'label' => 'Password Saat Ini',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-12">
                @component('components.input-password', [
                    'id' => 'cp-password-new',
                    'name' => 'password_new',
                    'label' => 'Password Baru',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-12">
                @component('components.input-password', [
                    'id' => 'cp-password-confirm',
                    'name' => 'password_confirm',
                    'label' => 'Konfirmasi Password Baru',
                    'required' => true,
                ])
                @endcomponent
            </div>
        </div>
        <span class="text-danger" id="span-change-password">Silakan lengkapi form ini</span>
    </div>
@endcomponent

@push('custum-js')
@endpush
