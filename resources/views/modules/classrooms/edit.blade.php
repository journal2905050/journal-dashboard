@component('components.modal', [
    'id' => 'modal-classroom-edit',
    'title' => 'Edit Murid',
    'size' => 'lg',
    'form' => [
        'id' => 'form-classroom-edit',
        'name' => 'form-classroom-edit',
        'method' => 'POST',
    ],
    'remove' => [
        'id' => 'form-classroom-remove'
    ]
])
    <div class="form-body">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                @component('components.input-select', [
                    'id' => 'classroom-school_year-edit',
                    'name' => 'school_year',
                    'required' => true,
                    'label' => 'Tahun Ajaran',
                    'class' => 'select-school-year form-select',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-select', [
                    'id' => 'classroom-grade-edit',
                    'name' => 'grade',
                    'label' => 'Tingkat',
                    'class' => 'select-grade form-select',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'classroom-name-edit',
                    'name' => 'name',
                    'label' => 'Nama',
                    'required' => true,
                ])
                @endcomponent
            </div>
        </div>
    </div>
@endcomponent

@push('custum-js')
    <script src="/app-js/classrooms/edit.js"></script>
@endpush
