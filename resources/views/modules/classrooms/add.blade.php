@component('components.modal', [
    'id' => 'modal-classroom-add',
    'title' => 'Tambah Kelas',
    'size' => 'md',
    'form' => [
        'id' => 'form-classroom-add',
        'name' => 'form-classroom-add',
        'method' => 'POST',
    ],
])
    <div class="form-body">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                @component('components.input-select', [
                    'id' => 'classroom-school_year-add',
                    'name' => 'school_year',
                    'required' => true,
                    'label' => 'Tahun Ajaran',
                    'class' => 'select-school-year form-select ',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-select', [
                    'id' => 'classroom-grade-add',
                    'name' => 'grade',
                    'label' => 'Tingkat',
                    'class' => 'select-grade form-select',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'classroom-name-add',
                    'name' => 'name',
                    'label' => 'Nama',
                    'required' => true,
                ])
                @endcomponent
            </div>
        </div>
    </div>
@endcomponent

@push('custum-js')
    <script src="/app-js/classrooms/add.js"></script>
@endpush
