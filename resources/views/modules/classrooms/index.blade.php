@extends('layouts.app')
@section('title', 'Kelas')

@section('content')

    @component('components.card')

        <div class="row justify-content-between">
            <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-6">
                        @component('components.input-select', [
                            'id' => 'classroom-select-school-year',
                            'class' => 'select-school-year',
                            'label' => 'Tahun Ajaran',
                        ])
                        @endcomponent
                    </div>
                    <div class="col-4">
                        @component('components.input-select', [
                            'id' => 'classroom-select-grade',
                            'class' => 'select-grade',
                            'label' => 'Tingkat',
                        ])
                        @endcomponent
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-4 col-sm-12"></div> --}}

            <div class="col-md-2 col-sm-12 text-right">
                <button id="btn-classroom-add" type="button" class="btn btn-icon btn-primary mr-1 mb-1 mt-2 btn-inline">
                    <i class="feather icon-plus"> Tambah Kelas</i>
                </button>
            </div>

        </div>

        @component('components.table-responsive', [
            'id' => 'table-classroom',
            'columns' => ['Action', 'Tahun Ajaran', 'Tingkat', 'Nama'],
        ])
        @endcomponent
    @endcomponent


    @component('modules.classrooms.add')
    @endcomponent

    @component('modules.classrooms.edit')
    @endcomponent


@endsection

@section('js')
    <script src="/app-js/classrooms/index.js"></script>
    @stack('custum-js')
@endsection
