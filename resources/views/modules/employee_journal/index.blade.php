@extends('layouts.app')
@section('title', 'Jurnal Saya')

@section('content')

    @component('components.card')

        <div class="row justify-content-between">
            <div class="col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        @component('components.input-date', [
                            'id' => 'employee_journal-select-date',
                            'class' => 'select-date date-now',
                            'label' => 'Tanggal',
                        ])
                        @endcomponent
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-4 col-sm-12"></div> --}}

            <div class="col-md-2 col-sm-12 text-right">
                <button id="btn-employee_journal-add" type="button" class="btn btn-icon btn-primary mr-1 mb-1 mt-2 btn-inline">
                    <i class="feather icon-plus"> Tambah Jurnal</i>
                </button>
            </div>

        </div>

        @component('components.table-responsive', [
            'id' => 'table-employee_journal',
            'columns' => ['Kelas', 'Tingkat', 'Mata Pelajaran', 'Jam Ke', 'Action'],
        ])
        @endcomponent
    @endcomponent


    @component('modules.employee_journal.add')
    @endcomponent

    @component('modules.employee_journal.edit')
    @endcomponent


@endsection

@section('js')
    <script src="/app-js/employee_journals/index.js"></script>
    @stack('custum-js')
@endsection
