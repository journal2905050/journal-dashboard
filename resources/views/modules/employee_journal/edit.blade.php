@component('components.modal', [
    'id' => 'modal-employee_journal-edit',
    'title' => 'Edit Murid',
    'size' => 'xl',
    // 'form' => [
    //     'id' => 'form-employee_journal-edit',
    //     'name' => 'form-employee_journal-edit',
    //     'method' => 'POST',
    // ],
    // 'remove' => [
    //     'id' => 'form-employee_journal-remove',
    // ]
])
    <form id="form-employee_journal-edit" method="post">
        @csrf
        <div class="form-body">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    @component('components.input-date', [
                        'id' => 'date-employee_journal-edit',
                        'name' => 'date',
                        'class' => 'select-date date-now',
                        'label' => 'Tanggal',
                        'required' => true,
                        'readonly' => true,
                    ])
                    @endcomponent
                </div>
                <div class="col-md-3 col-sm-12">
                    @component('components.input-text', [
                        'id' => 'school-year-employee_journal-edit',
                        'label' => 'Tahun Ajaran',
                        'readonly' => true,
                    ])
                    @endcomponent
                </div>
                <div class="col-md-3 col-sm-12">
                    @component('components.input-text', [
                        'id' => 'grade-employee_journal-edit',
                        'label' => 'Tingkat',
                        'readonly' => true,
                    ])
                    @endcomponent
                </div>
                <div class="col-md-3 col-sm-12">
                    @component('components.input-text', [
                        'id' => 'employee_journal-select-classroom-edit',
                        'label' => 'Kelas',
                        'readonly' => true,
                    ])
                    @endcomponent
                </div>
                <div class="col-md-3 col-sm-12">
                    @component('components.input-text', [
                        'id' => 'employee_journal-select-subject-edit',
                        'label' => 'Mata Pelajaran',
                        'readonly' => true,
                    ])
                    @endcomponent
                </div>
                <div class="col-md-3 col-sm-12">
                    @component('components.input-text', [
                        'id' => 'hour-employee_journal-edit',
                        'label' => 'Jam Ke',
                        'readonly' => true,
                    ])
                    @endcomponent
                </div>
                <div class="col-md-6 col-sm-12">
                    @component('components.input-textarea', [
                        'id' => 'summary-employee_journal-edit',
                        'name' => 'summary',
                        'label' => 'Summary',
                    ])
                    @endcomponent
                </div>
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-bordered" id="table-employee_journal-edit" 2>
                                <thead id="thead-employee_journal"></thead>
                                <tbody id="tbody-employee_journal"></tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <hr class="divider">
        <div class="mt-2 mb-1 ml-2 mr-2">
            <div class="row justify-content-between">
                <div class="col-2">
                    {{-- <button type="button" class="btn btn-danger" id="form-employee_journal-remove">Hapus</button> --}}
                </div>
                <div class="col-8 text-right">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="reset" class="btn btn-warning" data-dismiss="modal">Batal</button>
                </div>
            </div>

        </div>
    </form>

@endcomponent

@push('custum-js')
    <script src="/app-js/employee_journals/edit.js"></script>
@endpush
