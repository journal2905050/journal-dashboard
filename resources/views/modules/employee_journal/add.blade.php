@component('components.modal', [
    'id' => 'modal-employee_journal-add',
    'title' => 'Tambah Kelas',
    'size' => 'lg',
    'form' => [
        'id' => 'form-employee_journal-add',
        'name' => 'form-employee_journal-add',
        'method' => 'POST',
    ],
])
    <div class="form-body">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                @component('components.input-date', [
                    'id' => 'date-employee_journal-add',
                    'name' => 'date',
                    'class' => 'select-date date-now',
                    'label' => 'Tanggal',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-select', [
                    'id' => 'school-year-employee_journal-add',
                    'name' => 'school_year',
                    'class' => 'select-school-year form-select',
                    'label' => 'Tahun Ajaran',
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-select', [
                    'id' => 'grade-employee_journal-add',
                    'name' => 'grade',
                    'class' => 'select-grade form-select',
                    'label' => 'Tingkat',
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-select', [
                    'id' => 'employee_journal-select-classroom-add',
                    'class' => 'form-select',
                    'name' => 'classroom',
                    'label' => 'Kelas',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee_journal-select-subject-add',
                    'class' => 'form-select',
                    'name' => 'subject',
                    'label' => 'Mata Pelajaran',
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-select', [
                    'id' => 'hour-employee_journal-add',
                    'name' => 'hours[]',
                    'class' => 'select-hour form-select',
                    'label' => 'Jam Ke',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-12 col-sm-12">
                @component('components.input-textarea', [
                    'id' => 'summary-employee_journal-add',
                    'name' => 'summary',
                    'label' => 'Summary',
                ])
                @endcomponent
            </div>
        </div>
    </div>
@endcomponent

@push('custum-js')
    <script src="/app-js/employee_journals/add.js"></script>
@endpush
