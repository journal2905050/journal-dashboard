@extends('layouts.app')
@section('title', 'Karyawan')

@section('content')

    @component('components.card')

        <div class="row justify-content-end">
            <button id="btn-employee-add" type="button" class="btn btn-icon btn-primary mr-1 mb-1 btn-inline">
                <i class="feather icon-plus"> Tambah Karyawan</i>
            </button>
        </div>

        @component('components.table-responsive', [
            'id' => 'table-employee',
            'columns' => ['Action', 'NIP', 'Nama', 'Kode', 'Pangkat/Golongan', 'jabatan', 'Status'],
        ])
        @endcomponent
    @endcomponent


    @component('modules.employees.add')
    @endcomponent

    @component('modules.employees.edit')
    @endcomponent


@endsection

@section('js')
    <script src="/app-js/employees/index.js"></script>
    @stack('custum-js')
@endsection
