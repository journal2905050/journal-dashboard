@component('components.modal', [
    'id' => 'modal-employee-add',
    'title' => 'Tambah Karyawan',
    'size' => 'xl',
    'form' => [
        'id' => 'form-employee-add',
        'name' => 'form-employee-add',
        'method' => 'POST',
    ],
])
    <div class="form-body">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-name-add',
                    'name' => 'person[name]',
                    'label' => 'Nama',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-alias-add',
                    'name' => 'employee[alias]',
                    'label' => 'Kode',
                    'required' => false,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-nip-add',
                    'name' => 'employee[nip]',
                    'label' => 'NIP',
                    'required' => false,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-position-add',
                    'name' => 'employee[position]',
                    'label' => 'Jabatan',
                    'required' => false,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-rank-add',
                    'name' => 'employee[rank]',
                    'label' => 'Pangkat / Golongan',
                    'required' => false,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-status-add',
                    'name' => 'employee[status]',
                    'label' => 'Status',
                    'required' => false,
                ])
                @endcomponent
            </div>
        </div>
        <div class="divider divider-left">
            <div class="divider-text">User</div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-username-add',
                    'name' => 'user[username]',
                    'label' => 'Username',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-password-add',
                    'name' => 'user[password]',
                    'label' => 'Password',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-3 col-sm-12">
                @component('components.input-checkbox', [
                    'id' => 'employee-is_admin-add',
                    'name' => 'user[is_admin]',
                    'checked' => false,
                    'label' => 'Admin',
                    'class' => 'mt-2',
                ])
                @endcomponent
            </div>
            <div class="col-md-3 col-sm-12">
                @component('components.input-checkbox', [
                    'id' => 'employee-is_employee-add',
                    'name' => 'user[is_employee]',
                    'checked' => false,
                    'label' => 'Pengajar',
                    'class' => 'mt-2',
                ])
                @endcomponent
            </div>
            <div class="col-md-3 col-sm-12">
                @component('components.input-checkbox', [
                    'id' => 'employee-is_bk-add',
                    'name' => 'user[is_bk]',
                    'checked' => false,
                    'label' => 'BK',
                    'class' => 'mt-2',
                ])
                @endcomponent
            </div>
        </div>
    </div>
@endcomponent

@push('custum-js')
    <script src="/app-js/employees/add.js"></script>
@endpush
