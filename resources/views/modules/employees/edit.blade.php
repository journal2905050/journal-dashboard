@component('components.modal', [
    'id' => 'modal-employee-edit',
    'title' => 'Edit Karyawan',
    'size' => 'xl',
    'form' => [
        'id' => 'form-employee-edit',
        'name' => 'form-employee-edit',
        'method' => 'POST',
    ],
])
    <div class="form-body">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-name-edit',
                    'name' => 'person[name]',
                    'label' => 'Nama',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-alias-edit',
                    'name' => 'employee[alias]',
                    'label' => 'Kode',
                    'required' => false,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-nip-edit',
                    'name' => 'employee[nip]',
                    'label' => 'NIP',
                    'required' => false,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-position-edit',
                    'name' => 'employee[position]',
                    'label' => 'Jabatan',
                    'required' => false,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-rank-edit',
                    'name' => 'employee[rank]',
                    'label' => 'Pangkat / Golongan',
                    'required' => false,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-status-edit',
                    'name' => 'employee[status]',
                    'label' => 'Status',
                    'required' => false,
                ])
                @endcomponent
            </div>
        </div>
        <div class="divider divider-left">
            <div class="divider-text">User</div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-username-edit',
                    'name' => 'user[username]',
                    'label' => 'Username',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'employee-password-edit',
                    'name' => 'user[password]',
                    'label' => 'Password',
                    'required' => false,
                    'span' => [
                        'label' => 'Isikan jia ingin merubah password !!!',
                    ],
                ])
                @endcomponent
            </div>
            <div class="col-md-3 col-sm-12">
                @component('components.input-checkbox', [
                    'id' => 'employee-is_admin-edit',
                    'name' => 'user[is_admin]',
                    'checked' => false,
                    'label' => 'Admin',
                    'class' => 'mt-2',
                ])
                @endcomponent
            </div>
            <div class="col-md-3 col-sm-12">
                @component('components.input-checkbox', [
                    'id' => 'employee-is_employee-edit',
                    'name' => 'user[is_employee]',
                    'checked' => false,
                    'label' => 'Pengajar',
                    'class' => 'mt-2',
                ])
                @endcomponent
            </div>
            <div class="col-md-3 col-sm-12">
                @component('components.input-checkbox', [
                    'id' => 'employee-is_bk-edit',
                    'name' => 'user[is_bk]',
                    'checked' => false,
                    'label' => 'BK',
                    'class' => 'mt-2',
                ])
                @endcomponent
            </div>
        </div>
    </div>
@endcomponent

@push('custum-js')
    <script src="/app-js/employees/edit.js"></script>
@endpush
