@component('components.input-checkbox', [
    'id' => 'mutation-student-' . $model->id,
    'name' => 'students[]',
    'checked' => false,
    'value' => $model->id,
])
@endcomponent
