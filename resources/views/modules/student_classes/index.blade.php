@extends('layouts.app')
@section('title', 'Daftar Murid')

@section('content')

    @component('components.card')

        <div class="row justify-content-between">
            <div class="col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        @component('components.input-select', [
                            'id' => 'student_class-select-school-year',
                            'class' => 'select-school-year',
                            'label' => 'Tahun Ajaran',
                        ])
                        @endcomponent
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        @component('components.input-select', [
                            'id' => 'student_class-select-grade',
                            'class' => 'select-grade',
                            'label' => 'Tingkat',
                        ])
                        @endcomponent
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        @component('components.input-select', [
                            'id' => 'student_class-select-classroom',
                            'class' => '',
                            'label' => 'Kelas',
                        ])
                        @endcomponent
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-4 col-sm-12"></div> --}}

            <div class="col-md-2 col-sm-12 text-right">
                <button id="btn-student_class-mutation" type="button" class="btn btn-icon btn-primary mr-1 mb-1 mt-2 btn-inline">
                    <i class="feather icon-shuffle"> Mutasi</i>
                </button>
            </div>

        </div>

        @component('components.table-responsive', [
            'id' => 'table-student_class',
            'columns' => ['Nis', 'Nama', 'Kelas', 'Tingkat', 'Tahun Ajaran'],
        ])
        @endcomponent
    @endcomponent


    @component('modules.student_classes.mutation')
    @endcomponent

    {{-- @component('modules.student_classes.edit')
    @endcomponent --}}


@endsection

@section('js')
    <script src="/app-js/student_classes/index.js"></script>
    @stack('custum-js')
@endsection
