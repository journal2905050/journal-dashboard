@component('components.modal', [
    'id' => 'modal-student_class-mutation',
    'title' => 'Mutasi Murid',
    'size' => 'xl',
])
    <form id="form-student-mutation">
        @csrf
        <div class="divider divider-left">
            <div class="divider-text">Tujuan mutasi</div>
        </div>
        <div class="form-body">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    @component('components.input-select', [
                        'id' => 'student_class-school_year-mutation',
                        'name' => 'school_year',
                        'required' => true,
                        'label' => 'Tahun Ajaran',
                        'class' => 'select-school-year form-select',
                        'required' => true,
                    ])
                    @endcomponent
                </div>
                <div class="col-md-4 col-sm-12">
                    @component('components.input-select', [
                        'id' => 'student_class-grade-mutation',
                        'name' => 'grade',
                        'label' => 'Tingkat',
                        'class' => 'select-grade form-select',
                        'required' => true,
                    ])
                    @endcomponent
                </div>
                <div class="col-md-4 col-sm-12">
                    @component('components.input-select', [
                        'id' => 'student_class-classroom-mutation',
                        'name' => 'classroom',
                        'required' => true,
                        'label' => 'Kelas tujuan',
                        'class' => 'form-select',
                        'required' => true,
                    ])
                    @endcomponent
                </div>
            </div>
        </div>
        <div class="divider divider-left">
            <div class="divider-text">Pilih murid yang akan dimutasi</div>
        </div>
        <div class="form-body">
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    @component('components.input-select', [
                        'id' => 'student_class-batch-in-mutation-search',
                        'name' => 'batch_in',
                        'label' => 'Tingkat',
                        'class' => 'select-batch-in-exist form-select',
                        'required' => true,
                    ])
                    @endcomponent
                </div>

            </div>

            <div class="mx-1">
                @component('components.table-responsive', [
                    'id' => 'table-student-mutation',
                    'columns' => ['Pilih', 'NIS', 'Nama', 'Kelas'],
                ])
                @endcomponent
            </div>

            <hr class="divider">
            <div class="mt-2 mb-1 ml-2 mr-2">
                <div class="row justify-content-between">
                    <div class="col-2">

                    </div>
                    <div class="col-8 text-right">
                        <button type="submit" class="btn btn-success">Simpan</button>
                        <button type="reset" class="btn btn-warning" data-dismiss="modal">Batal</button>
                    </div>
                </div>

            </div>
        </div>
    </form>

@endcomponent

@push('custum-js')
    <script src="/app-js/student_classes/mutation.js"></script>
@endpush
