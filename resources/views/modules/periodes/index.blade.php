@extends('layouts.app')
@section('title', 'Periode Akademik')

@section('content')

    @component('components.card')

        <div class="row justify-content-end">
            <button id="btn-periode-add" type="button" class="btn btn-icon btn-primary mr-1 mb-1 btn-inline">
                <i class="feather icon-plus"> Tambah Periode</i>
            </button>
        </div>

        @component('components.table-responsive', [
            'id' => 'table-periode',
            'columns' => ['Tahun Ajaran', 'Aktif', 'Action'],
        ])
        @endcomponent
    @endcomponent

    @component('modules.periodes.add')
    @endcomponent

@endsection

@section('js')
    <script src="/app-js/periodes/index.js"></script>
    @stack('custum-js')
@endsection
