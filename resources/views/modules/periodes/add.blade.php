@component('components.modal', [
    'id' => 'modal-periode-add',
    'title' => 'Tambah Periode',
    'size' => 'md',
    'form' => [
        'id' => 'form-periode-add',
        'name' => 'form-periode-add',
        'method' => 'POST',
    ],
    'styleBody' => 'min-height: 400px;',
])
    <div class="form-body">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                @component('components.input-text', [
                    'id' => 'periode-school_year_add',
                    'name' => 'school_year',
                    'label' => 'Tahun Ajaran',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-4 col-sm-12">
                @component('components.input-checkbox', [
                    'id' => 'periode-is_active-add',
                    'name' => 'is_active',
                    'checked' => false,
                    'label' => 'Is Active',
                    'class' => 'mt-2',
                ])
                @endcomponent
            </div>
        </div>
    </div>
@endcomponent

@push('custum-js')
    <script src="/app-js/periodes/add.js"></script>
@endpush
