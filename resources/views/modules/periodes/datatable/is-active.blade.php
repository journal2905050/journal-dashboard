@component('components.input-checkbox', [
    'id' => 'is-active' . $model->id,
    'checked' => $model->is_active,
    'onchange' => 'setActivePeriode("' . $model->id . '")',
])
@endcomponent
