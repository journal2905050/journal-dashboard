<button type="button" class="btn btn-sm btn-icon btn-outline-danger mr-1 mb-1 btn-inline" data-toggle="tooltip"
    data-placement="top" title="Hapus">
    <i class="feather icon-trash-2" onclick="deletePeriode('{{ $model->id }}')"></i>
</button>
