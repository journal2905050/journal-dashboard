@extends('layouts.app')
@section('title', 'Presensi')

@section('content')

    @component('components.card')

        <div class="row justify-content-between">
            <div class="col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        @component('components.input-select', [
                            'id' => 'bk_presence-select-school-year',
                            'class' => 'select-school-year',
                            'label' => 'Tahun Ajaran',
                        ])
                        @endcomponent
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        @component('components.input-select', [
                            'id' => 'bk_presence-select-grade',
                            'class' => 'select-grade',
                            'label' => 'Tingkat',
                        ])
                        @endcomponent
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        @component('components.input-select', [
                            'id' => 'bk_presence-select-classroom',
                            'class' => '',
                            'label' => 'Kelas',
                        ])
                        @endcomponent
                    </div>
                    <div class="col-md-3 col-sm-12">
                        @component('components.input-date', [
                            'id' => 'bk_presence-select-date',
                            'class' => 'select-date date-now',
                            'label' => 'Tanggal',
                        ])
                        @endcomponent
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-3 col-sm-12"></div> --}}

            <div class="col-md-2 col-sm-12 text-right">
                {{-- <button id="btn-bk_presence-add" type="button" class="btn btn-icon btn-primary mr-1 mb-1 mt-2 btn-inline">
                    <i class="feather icon-plus"> Tambah Jurnal</i>
                </button> --}}
            </div>

        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-hover-animation" id="table-bk_presence">
                <thead>
                    <tr>
                        <th rowspan="2" class="align-middle text-center">Action</th>
                        <th rowspan="2" class="align-middle text-center">NIS</th>
                        <th rowspan="2" class="align-middle text-center">Nama</th>
                        <th rowspan="2" class="align-middle text-center">Presentase Kehadiran</th>
                        <th colspan="12" class="text-center">Jam Ke</th>
                    </tr>
                    <tr>
                        <th class="text-center">1</th>
                        <th class="text-center">2</th>
                        <th class="text-center">3</th>
                        <th class="text-center">4</th>
                        <th class="text-center">5</th>
                        <th class="text-center">6</th>
                        <th class="text-center">7</th>
                        <th class="text-center">8</th>
                        <th class="text-center">9</th>
                        <th class="text-center">10</th>
                        <th class="text-center">11</th>
                        <th class="text-center">12</th>
                    </tr>
                </thead>
                <tbody id="tbody-bk_presence"></tbody>
            </table>
        </div>
    @endcomponent


    {{-- @component('modules.bk_presence.add')
    @endcomponent --}}

    @component('modules.bk_presences.edit')
    @endcomponent


@endsection

@section('js')
    <script src="/app-js/bk_presences/index.js"></script>
    @stack('custum-js')
@endsection
