@component('components.modal', [
    'id' => 'modal-presence-edit',
    'title' => 'Edit Presensi',
    'size' => 'lg',
])
    <form id="form-presence-edit">
        @csrf
        <div class="form-body">
            <div class="row">
                <div class="col-md-2 col-sm-12">
                    @component('components.input-text', [
                        'id' => 'presence-nis-edit',
                        'label' => 'NIS',
                        'readonly' => true,
                    ])
                    @endcomponent
                </div>
                <div class="col-md-6 col-sm-12">
                    @component('components.input-text', [
                        'id' => 'presence-name-edit',
                        'label' => 'Nama',
                        'readonly' => true,
                    ])
                    @endcomponent
                </div>
                <div class="col-md-4 col-sm-12">
                    @component('components.input-date', [
                        'id' => 'presence-date-edit',
                        'label' => 'Tanggal',
                        'name' => 'date',
                        'readonly' => true,
                    ])
                    @endcomponent
                </div>
            </div>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Jam Ke</th>
                            <th>Presensi</th>
                            <th>Jadwal</th>
                            <th>Guru</th>
                            <th>Kelas</th>
                        </tr>
                    </thead>
                    <tbody id="tbody-bk_presence-show"></tbody>
                </table>
            </div>

            <hr class="divider">
            <div class="mt-2 mb-1 ml-2 mr-2">
                <div class="row justify-content-between">
                    <div class="col-2">
                        {{-- <button type="button" class="btn btn-danger" id="form-employee_journal-remove">Hapus</button> --}}
                    </div>
                    <div class="col-8 text-right">
                        @if (Auth::user()->is_bk)
                            <button type="submit" class="btn btn-success">Simpan</button>
                        @endif
                        <button type="reset" class="btn btn-warning" data-dismiss="modal">Batal</button>
                    </div>
                </div>

            </div>
        </div>
    </form>
@endcomponent

@push('custum-js')
    <script src="/app-js/bk_presences/edit.js"></script>
@endpush
