@component('components.modal', [
    'id' => 'modal-student-edit',
    'title' => 'Edit Murid',
    'size' => 'xl',
    'form' => [
        'id' => 'form-student-edit',
        'name' => 'form-student-edit',
        'method' => 'POST',
    ],
])
    <div class="form-body">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'student-name-edit',
                    'name' => 'person[name]',
                    'label' => 'Nama',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-select', [
                    'id' => 'student-gender-edit',
                    'name' => 'person[gender]',
                    'required' => true,
                    'label' => 'Jenis Kelamin',
                    'class' => 'select-gender form-select'
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'student-nis-edit',
                    'name' => 'student[nis]',
                    'label' => 'NIS',
                    'required' => false,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-number', [
                    'id' => 'student-batch_in-edit',
                    'name' => 'student[batch_in]',
                    'label' => 'Tahun Masuk',
                    'required' => true,
                    'min' => 1945,
                    'max' => 9999,
                ])
                @endcomponent
            </div>
        </div>
        <div class="divider divider-left">
            <div class="divider-text">User</div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'student-username-edit',
                    'name' => 'user[username]',
                    'label' => 'Username',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'student-password-edit',
                    'name' => 'user[password]',
                    'label' => 'Password',
                    'required' => false,
                    'span' => [
                        'label' => 'Isikan jia ingin merubah password !!!',
                    ],
                ])
                @endcomponent
            </div>
        </div>
    </div>
@endcomponent

@push('custum-js')
    <script src="/app-js/students/edit.js"></script>
@endpush
