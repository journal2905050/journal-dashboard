@extends('layouts.app')
@section('title', 'Murid')

@section('content')

    @component('components.card')

        <div class="row justify-content-end">
            <button id="btn-student-add" type="button" class="btn btn-icon btn-primary mr-1 mb-1 btn-inline">
                <i class="feather icon-plus"> Tambah Murid</i>
            </button>
        </div>

        @component('components.table-responsive', [
            'id' => 'table-student',
            'columns' => ['Action', 'NIS', 'Nama','Jenis Kelamin', 'Tahun Masuk'],
        ])
        @endcomponent
    @endcomponent


    @component('modules.students.add')
    @endcomponent

    @component('modules.students.edit')
    @endcomponent


@endsection

@section('js')
    <script src="/app-js/students/index.js"></script>
    @stack('custum-js')
@endsection
