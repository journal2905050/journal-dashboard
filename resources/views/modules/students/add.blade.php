@component('components.modal', [
    'id' => 'modal-student-add',
    'title' => 'Tambah Karyawan',
    'size' => 'xl',
    'form' => [
        'id' => 'form-student-add',
        'name' => 'form-student-add',
        'method' => 'POST',
    ],
])
    <div class="form-body">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'student-name-add',
                    'name' => 'person[name]',
                    'label' => 'Nama',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-select', [
                    'id' => 'student-gender-add',
                    'name' => 'person[gender]',
                    'required' => true,
                    'label' => 'Jenis Kelamin',
                    'class' => 'select-gender form-select'
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'student-nis-add',
                    'name' => 'student[nis]',
                    'label' => 'NIS',
                    'required' => false,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-number', [
                    'id' => 'student-batch_in-add',
                    'name' => 'student[batch_in]',
                    'label' => 'Tahun Masuk',
                    'required' => true,
                    'min' => 1945,
                    'max' => 9999,
                ])
                @endcomponent
            </div>
        </div>
        <div class="divider divider-left">
            <div class="divider-text">User</div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'student-username-add',
                    'name' => 'user[username]',
                    'label' => 'Username',
                    'required' => true,
                ])
                @endcomponent
            </div>
            <div class="col-md-6 col-sm-12">
                @component('components.input-text', [
                    'id' => 'student-password-add',
                    'name' => 'user[password]',
                    'label' => 'Password',
                    'required' => true,
                ])
                @endcomponent
            </div>
        </div>
    </div>
@endcomponent

@push('custum-js')
    <script src="/app-js/students/add.js"></script>
@endpush
