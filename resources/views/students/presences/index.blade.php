@extends('layouts.app')
@section('title', 'Presence')

@section('content')

    @component('components.card')
        <div class="row justify-content-end">
            @component('components.input-month', [
                'class' => 'month-now',
                'id' => 'student-presence-month',
                'label' => 'Bulan',
            ])
            @endcomponent
        </div>

        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Action</th>
                        <th>Tanggal</th>
                        <th>Masuk</th>
                        <th>Ijin</th>
                        <th>Terlambat</th>
                        <th>Aplha</th>
                        <th>Persentase Kehadiran</th>
                    </tr>
                </thead>
                <tbody id="tbody-student-presence"></tbody>
            </table>
        </div>
    @endcomponent

    @component('students.presences.edit')
    @endcomponent


@endsection

@section('js')
    <script src="/app-js/student_presences/index.js"></script>
    <script>
        presence_student_id = "{{ Auth::user()->id }}"
    </script>
    @stack('custum-js')
@endsection
