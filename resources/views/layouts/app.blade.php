<!DOCTYPE html>
<html class="loading" lang="{{ str_replace('_', '-', app()->getLocale()) }}" data-textdirection="ltr">
<!-- BEGIN: Head-->
@include('layouts.components.head')
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static   menu-collapsed"
    data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">

    <!-- BEGIN: Header-->
    @component('layouts.components.header-navbar')
    @endcomponent
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">

        @component('layouts.components.navbar-header')
        @endcomponent

        <div class="main-menu-content">
            @component('layouts.components.navigation')
            @endcomponent
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            {{-- <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                        </div>
                    </div>
                </div>
            </div> --}}
            <div class="content-body">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    @include('layouts.components.foot')
    <!-- END: Footer-->

    @component('modules.users.change_password')
    @endcomponent

    <script>
        let main_url = '{{ url('') }}';
        var _token = '{{ csrf_token() }}';
    </script>


    <!-- BEGIN: Vendor JS-->
    <script src="/themes/app-assets/vendors/js/vendors.min.js"></script>
    <script src="/themes/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="/themes/app-assets/js/core/app-menu.js"></script>
    <script src="/themes/app-assets/js/core/app.js"></script>
    <script src="/themes/app-assets/js/scripts/components.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="/themes/app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
    <script src="/themes/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="/themes/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js"></script>
    <script src="/themes/app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
    <script src="/themes/app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
    <script src="/themes/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
    <script src="/themes/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>

    {{-- Adaption --}}
    <script src="/app-js/core/dropdown.js"></script>
    <script src="/app-js/core/app.js"></script>

    @yield('js')
    <script src="/app-js/core/change-password.js"></script>

    <script src="/themes/app-assets/vendors/js/extensions/toastr.min.js"></script>

    <!-- END: Page JS-->


</body>
<!-- END: Body-->

</html>
