<div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
        <li class="nav-item mr-auto"><a class="navbar-brand" href="">
                {{-- <div class="brand-logo"></div> --}}
                <div><img src="/images/logo.png" alt="" width="40px" height="40px"></div>
                <h2 class="brand-text mb-0">SMA N 1 <br>Ambarawa</h2>
            </a></li>
        <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i
                    class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i
                    class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary"
                    data-ticon="icon-disc"></i></a></li>
    </ul>
</div>
<div class="shadow-bottom"></div>
