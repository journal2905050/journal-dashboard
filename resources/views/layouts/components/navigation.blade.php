<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
    @if (!Auth::user()->is_student)
        <li class=" nav-item {{ Request::segment(1) == '' ? 'active' : null }}">
            <a href="{{ url('') }}"><i class="feather icon-home"></i>
                <span class="menu-title" data-i18n="Dashboard">Dashboard</span>
            </a>
        </li>
    @endif

    @if (Auth::user()->is_admin)
        <li class=" navigation-header"><span>Master</span></li>

        <li class=" nav-item {{ Request::segment(1) == 'periode' ? 'active' : null }}">
            <a href="{{ url('periode') }}"><i class="feather icon-calendar"></i>
                <span class="menu-title" data-i18n="Periode Akademik">Periode Akademik</span>
            </a>
        </li>

        <li class="nav-item"><a href="#"><i class="feather icon-users"></i>
                <span class="menu-title" data-i18n="Pengguna">Pengguna</span></a>
            <ul class="menu-content">
                <li class="{{ Request::segment(1) == 'employee' ? 'active' : null }}">
                    <a href="{{ url('employee') }}">
                        <i class="feather icon-circle"></i>
                        <span class="menu-title" data-i18n="Karyawan">Karyawan</span>
                    </a>
                </li>
                <li class="{{ Request::segment(1) == 'student' ? 'active' : null }}">
                    <a href="{{ url('student') }}">
                        <i class="feather icon-circle"></i>
                        <span class="menu-title" data-i18n="Murid">Murid</span>
                    </a>
                </li>
            </ul>

        <li class="nav-item"><a href="#"><i class="feather icon-layers"></i>
                <span class="menu-title" data-i18n="Kelas">Kelas</span></a>
            <ul class="menu-content">
                <li class="{{ Request::segment(1) == 'classroom' ? 'active' : null }}">
                    <a href="{{ url('classroom') }}">
                        <i class="feather icon-circle"></i>
                        <span class="menu-title" data-i18n="Daftar Kelas">Daftar Kelas</span>
                    </a>
                </li>
                <li class="{{ Request::segment(1) == 'student-class' ? 'active' : null }}">
                    <a href="{{ url('student-class') }}">
                        <i class="feather icon-circle"></i>
                        <span class="menu-title" data-i18n="Daftar Murid">Daftar Murid</span>
                    </a>
                </li>
            </ul>
        </li>
    @endif

    @if (Auth::user()->is_employee || Auth::user()->is_bk)
        <li class=" navigation-header"><span>Journal & Presensi</span></li>
    @endif
    @if (Auth::user()->is_employee)
        <li class=" nav-item {{ Request::segment(1) == 'employee-journal' ? 'active' : null }}">
            <a href="{{ url('employee-journal') }}"><i class="feather icon-list"></i>
                <span class="menu-title" data-i18n="Journal Saya">Journal Saya</span>
            </a>
        </li>
    @endif
    @if (Auth::user()->is_employee || Auth::user()->is_bk)
        <li class=" nav-item {{ Request::segment(1) == 'bk-presence' ? 'active' : null }}">
            <a href="{{ url('bk-presence') }}"><i class="feather icon-user-check"></i>
                <span class="menu-title" data-i18n="Presensi">Presensi</span>
            </a>
        </li>
    @endif
    @if (Auth::user()->is_student)
        <li class=" nav-item {{ Request::segment(1) == '' ? 'active' : null }}">
            <a href="{{ url('') }}"><i class="feather icon-home"></i>
                <span class="menu-title" data-i18n="Dashboard">Dashboard</span>
            </a>
        </li>
        <li class=" nav-item {{ Request::segment(1) == 'student-presence' ? 'active' : null }}">
            <a href="{{ url('student-presence') }}"><i class="feather icon-user-check"></i>
                <span class="menu-title" data-i18n="Presensi">Presensi</span>
            </a>
        </li>
    @endif



</ul>
