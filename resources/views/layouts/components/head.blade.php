<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description"
        content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords"
        content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="ADWILAPA">
    <title>SMAN 1 Ambarawa | @yield('title')
    </title>
    <link rel="apple-touch-icon" href="/themes/app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="/themes/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/themes/app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css"href="/themes/app-assets/vendors/css/forms/select/select2.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/themes/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/themes/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/themes/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/themes/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/themes/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/themes/app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/themes/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="/themes/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="/themes/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="/themes/app-assets/vendors/css/extensions/toastr.css">
    <link rel="stylesheet" type="text/css" href="/themes/assets/css/style.css">
    <!-- END: Custom CSS-->

    <link rel="stylesheet" type="text/css" href="/themes/app-assets/css/plugins/extensions/toastr.css">

    @yield('css')

</head>
